## v0.3.0
Date: March 30, 2022

### Added

- Float features for infinity

### Removed

- `mtk-utils`

## v0.2.1 - December 13, 2020

### Changed

- Isolate FiveAM usage to test packages only. Removed explicit FiveAM
  requirement from clpmfile (should be automatically pulled).
- Refactor tests and test packages to separate out tests that require
  symbols which are internal to the source package (i.e. non-exported).

## v0.2.0 - July 24, 2020

### Added

- CLPM support
