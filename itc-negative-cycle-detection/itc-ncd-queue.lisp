(in-package :itc-v2)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Q IMPLEMENTATION
;; - In support of Negative Cycle Detection BFM-Graph
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass queue ()
  ((type
    :type symbol
    :initform :fifo
    :initarg :type
    :accessor queue-type
    :documentation "A symbol indicating whether this queue is FIFO (:fifo) or LIFO (:lifo).")
   (head
    :type (or null bfm-vertex)
    :initform nil
    :initarg :head
    :accessor queue-head
    :documentation "")
   (tail
    :type (or null bfm-vertex)
    :initform nil
    :initarg :tail
    :accessor queue-tail
    :documentation "")
   (dummy-head
    :type (or null bfm-vertex)
    :initform nil
    :initarg :dummy-head
    :accessor queue-dummy-head
    :documentation "")
   (dummy-tail
    :type (or null bfm-vertex)
    :initform nil
    :initarg :dummy-tail
    :accessor queue-dummy-tail
    :documentation "")))

(defun make-queue (&key (type :fifo))
  (let ((dummy-head (make-bfm-vertex :id :dummy-head))
        (dummy-tail (make-bfm-vertex :id :dummy-tail)))
    (setf (next-set-vertex dummy-head) dummy-tail)
    (make-instance 'queue
                   :type type
                   :head dummy-head :tail dummy-head
                   :dummy-head dummy-head :dummy-tail dummy-tail)))

(declaim (inline queue-participant?))
(defun queue-participant? (v)
  "Returns t if the V is participating in the queue.
   A vertex is a participant if its next-set-vertex pointer is not nil."
  (not (null (next-set-vertex v))))

(defmethod print-object ((q queue) stream)
  (format stream "(")
  (let ((v (next-set-vertex (queue-head q))))
    (loop while v do
         (when (and (queue-participant? v) (in-set v))
           (format stream "~a "(id v)))
         (setf v (next-set-vertex v))))
  (format stream ")"))

(defmethod debug-print ((q queue) &optional (stream t))
  "Print all the data about the queue:
   * Head pointer marked by H
   * Tail pointer marked by T
   * Lazily-removed elements marked by *."
  (with-output-to-format stream
    (format stream "(")
    (let ((v (queue-head q)))
      (loop while v do
           (if (eq v (queue-head q))
               (format stream "H->"))
           (if (eq v (queue-tail q))
               (format stream "T->"))
           (format stream "~a"(id v))
           (if (not (in-set v))
               (format stream "*"))
           (format stream " ")
           (setf v (next-set-vertex v))))
    (format stream ")")))

(declaim (inline cleanup-lazy-removals))
(defun cleanup-lazy-removals (q)
  "Traverses from the head of the queue to the
   first vertex v for which (in-set v) is true.
   Removes any vertices discovered to be lazily removed.
   Returns the vertex v, which could be nil if the list is empty."
  (let ((v (next-set-vertex (queue-head q))))
    ;; dequeue any nodes that were lazily marked for removal from the q.
    (loop while (and v (not (in-set v)) (queue-participant? v)) do
       ;; move the head/dummy vertex pointer to the next vertex
         (setf (next-set-vertex (queue-head q)) (next-set-vertex v))
       ;; remove the vertex
         (setf (next-set-vertex v) nil)
       ;; move v to the next vertex
         (setf v (next-set-vertex (queue-head q))))
    ;; update the tail
    (unless (queue-participant? v)
      (setf (queue-tail q) (queue-head q)))v))

;; FIX:
;; o BCW - INLINE fails for this method.  Declaim removed until fixed.
;;         May be interfering with an enqueue! method defined elsewhere.
;; (declaim (inline enqueue!))
(defmethod enqueue! ((q queue) (v bfm-vertex))
  (case (queue-type q)
    (:lifo (stack-add! q v))
    (:fifo (queue-add! q v))))

(declaim (inline stack-add!))
(defun stack-add! (s v)
  "Adds vertex to the head of the stack."
  (setf (in-set v) t)
  (unless (queue-participant? v)
    (setf (next-set-vertex v) (next-set-vertex (queue-head s)))
    (setf (next-set-vertex (queue-head s)) v)))

(declaim (inline queue-add!))
(defun queue-add! (q v)
  "Adds vertex to the tail of the queue."
  ;; (format t "Inserting ~a into Q~%" (id v))

  (setf (in-set v) t)
  (unless (queue-participant? v)
    (setf (next-set-vertex v) (next-set-vertex (queue-tail q)))
    (setf (next-set-vertex (queue-tail q)) v)
    (setf (queue-tail q) v)))

;; FIX:
;; o BCW - INLINE fails for this method.  Declaim removed until fixed.
;;         May be interfering with a dequeue! method defined elsewhere.
;;(declaim (inline dequeue!))
(defmethod dequeue! ((q queue))
  "Removes and returns an element from the head of the stack/queue.
   Accounts for vertices that were lazily removed,
   i.e. by setting (in-set v) to nil."
  (let ((v (cleanup-lazy-removals q)))
    ;; remove the first vertex still in the set
    (if (queue-participant? v)
        (progn
          ;; move the head/dummy vertex pointer to the next vertex
          (setf (next-set-vertex (queue-head q)) (next-set-vertex v))
          ;; remove the vertex
          (setf (in-set v) nil)
          (setf (next-set-vertex v) nil)
          v)
        nil)))

;; FIX:
;; o BCW - INLINE fails for this method.  Declaim removed until fixed.
;;         May be interfering with an isempty? method defined elsewhere.
;; (declaim (inline isempty?))
(defmethod isempty? ((q queue))
  "Returns t if the queue is empty, nil otherwise.
   Accounts for vertices that were lazily removed,
   i.e. by setting (in-set v) to nil."
  ;; test if there is a next-set-vertex that is in the set.
  (not (queue-participant? (cleanup-lazy-removals q))))

(declaim (inline empty!))
(defmethod empty! ((q queue))
  "Empties the queue."
  (let ((v (next-set-vertex (queue-head q)))
        v-next)
    ;; dequeue any nodes participating in the q
    (loop while (and v (queue-participant? v)) do
       ;; save off the next vertex
         (setf v-next (next-set-vertex v))
       ;; remove the vertex
         (setf (in-set v) nil)
         (setf (next-set-vertex v) nil)
       ;; move to the next vertex
         (setf v v-next)))
  (setf (queue-head q) (queue-dummy-head q))
  (setf (queue-tail q) (queue-dummy-head q))
  (setf (next-set-vertex (queue-head q)) (queue-dummy-tail q)))

(defmethod tolist ((q queue))
  "Returns a list representation of the contents of the queue."
  (let ((v (queue-head q)))
    ;; (format t "dummy-head: ~s~%" (queue-dummy-head q))
    ;; (format t "dummy-tail: ~s~%" (queue-dummy-head q))
    (loop while v
       when (in-set v) collect v
       do (setf v (next-set-vertex v)))))
