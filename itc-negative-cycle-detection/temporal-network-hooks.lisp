;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :itc-v2)

(defmethod itc-initialize! ((itc itc-negative-cycle-detection) &key (type-of-a-queue :fifo) (type-of-b-queue :fifo) &allow-other-keys)
  (setf (itc-ncd-graph itc) (make-bfm-graph :set-a-type type-of-a-queue :set-b-type type-of-b-queue))
  (initialize (itc-ncd-graph itc))
  ;; loop over any edges already in the temporal network and add them
  (with-slots (temporal-network) itc
    (do-constraints (c temporal-network)
      (itc-ncd-add-temporal-constraint! itc temporal-network c)))
  ;; Register hooks with the temporal-network.
  (add-itc-callbacks! itc))

(defmethod temporal-network-consistent? ((itc itc-negative-cycle-detection) &key &allow-other-keys)
  "Checks if `TEMPORAL-NETWORK` is consistent. Returns a `VALUES`
   structure where the first element is a boolean and the second is a
   list representing the negative cycle of the form:
   ((temp-const-obj :lb) (temp-const-obj :ub) ... ).
   The list represents whether the lowerbound or upperbound of
   a particular temporal constraint was involved in the cycle."
  (let ((cm (itc-ncd-constraint-map itc))
        consistent event-based-neg-cycle constraint-based-neg-cycle)
    ;; check consistency
    (multiple-value-setq (consistent event-based-neg-cycle)
      (check-consistency (itc-ncd-graph itc)))
    ;; convert a list of the form
    (setf constraint-based-neg-cycle
          (mapcar (lambda (event-pair)
                    (cl-heap:peep-at-heap (map-get cm event-pair)))
                  event-based-neg-cycle))
    ;; return
    (values consistent constraint-based-neg-cycle)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; CALLBACKS!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod add-itc-callbacks! ((itc itc-negative-cycle-detection))
  (let ((network (itc-temporal-network itc)))
    (tn:add-constraint-added-handler network #'itc-ncd-add-temporal-constraint! :source itc :provide-source-to-handler? t)))

(defmethod itc-ncd-add-temporal-constraint! ((itc itc-negative-cycle-detection) (network temporal-network) (c simple-temporal-constraint))
  ;; create constraint
  (let ((start-id (tn:id (tn:from-event c)))
        (end-id (tn:id (tn:to-event c)))
        (g (itc-ncd-graph itc))
        (cm (itc-ncd-constraint-map itc))
        (ub-handler nil)
        (lb-handler nil)
        (removed-handler nil))
    (let* ((node-ub (add-edge-with-constraint g start-id end-id cm c :ub))
           (node-lb (add-edge-with-constraint g end-id start-id cm c :lb))
           (node-ub-ptr (tg:make-weak-pointer node-ub))
           (node-lb-ptr (tg:make-weak-pointer node-lb)))
      ;; register lb callback
      (setf lb-handler
            (tn:add-lower-bound-changed-handler
             c #'(lambda (itc constraint old-value new-value)
                   (update-edge-with-constraint (itc-ncd-graph itc)
                                                end-id start-id
                                                (itc-ncd-constraint-map itc)
                                                constraint
                                                :lb old-value new-value
                                                (list (tg:weak-pointer-value node-lb-ptr))))
             :source itc :provide-source-to-handler? t))
      ;; register ub callback
      (setf ub-handler
            (tn:add-upper-bound-changed-handler
             c #'(lambda (itc constraint old-value new-value)
                   (update-edge-with-constraint (itc-ncd-graph itc)
                                                start-id end-id
                                                (itc-ncd-constraint-map itc)
                                                constraint
                                                :ub old-value new-value
                                                (list (tg:weak-pointer-value node-ub-ptr))))
             :source itc :provide-source-to-handler? t))
      ;; register removal callback
      (setf removed-handler
            (tn:add-constraint-removed-handler
             c #'(lambda (itc network constraint)
                   (declare (ignore network))
                   (remove-edge-with-constraint (itc-ncd-graph itc)
                                                start-id end-id
                                                (itc-ncd-constraint-map itc)
                                                (list (tg:weak-pointer-value node-ub-ptr)))
                   (remove-edge-with-constraint (itc-ncd-graph itc)
                                                end-id start-id
                                                (itc-ncd-constraint-map itc)
                                                (list (tg:weak-pointer-value node-lb-ptr)))
                   (tn:remove-lower-bound-changed-handler constraint lb-handler)
                   (tn:remove-upper-bound-changed-handler constraint ub-handler)
                   (tn:remove-constraint-removed-handler constraint removed-handler))
             :source itc :provide-source-to-handler? t)))))

(defmethod add-edge-with-constraint (g start-id end-id constraint-map constraint type)
  (let ((cm-q (map-get constraint-map `(,start-id ,end-id))))
    ;; create the heap if one has not been created.
    (unless cm-q
      (setf cm-q (make-instance 'cl-heap:fibonacci-heap :sort-fun #'< :key #'get-bound-from-record))
      (map-add! constraint-map `(,start-id ,end-id) cm-q))
    ;; add the constraint
    (multiple-value-bind (record node)
        (cl-heap:add-to-heap  cm-q `(,constraint ,type))
      (declare (ignore record))
      ;; modify the edges as needed
      (let ((min-record (cl-heap:peep-at-heap cm-q)))
        (modify-edge g start-id end-id (get-bound-from-record min-record))
        node))))

(defmethod remove-edge-with-constraint (g start-id end-id constraint-map heap-node)
  (let ((cm-q (map-get constraint-map `(,start-id ,end-id))))
    ;; only do something if there is a heap
    (when cm-q
      ;; remove the constraint
      (cl-heap:delete-from-heap cm-q (car heap-node))
      ;; remove the heap if it is empty
      (when (cl-heap:is-empty-heap-p cm-q)
        (map-remove! constraint-map `(,start-id ,end-id)))
      ;; modify the edges as needed
      (let ((min-record (cl-heap:peep-at-heap cm-q)))
        (if min-record
            (modify-edge g start-id end-id (get-bound-from-record min-record))
            (modify-edge g start-id end-id +inf+))))))

(defmethod update-edge-with-constraint (g start-id end-id constraint-map constraint type old-value new-value heap-node)
  (let ((cm-q (map-get constraint-map `(,start-id ,end-id))))
    ;; only do something if there is a heap
    (when cm-q
      ;; update the constraint in the heap
      (cond (;; do nothing if nothing has changed
             (= new-value old-value))
            (;; decrease the key
             (< new-value old-value)
             ;; decrease-key is an amortized O(1) operation,
             ;; but it has a check that assumes the value of heap-node
             ;; has not yet been decreased yet.
             ;; (cl-heap:decrease-key cm-q (car heap-node) new-value)

             ;; The method of removing the node and adding it has
             ;; O(logn) run time, but avoids the implementation
             ;; check described above.
             ;; TODO: The solution could be to either remove the check
             ;; from the fibbonacci heap library, or restore the
             ;; old-value to the constraint object.
             (cl-heap:delete-from-heap cm-q (car heap-node))
             (multiple-value-bind (record node)
                 (cl-heap:add-to-heap cm-q `(,constraint ,type))
               (declare (ignore record))
               (setf (car heap-node) node)))
            (;; increase the key
             :otherwise
             (cl-heap:delete-from-heap cm-q (car heap-node))
             (multiple-value-bind (record node)
                 (cl-heap:add-to-heap cm-q `(,constraint ,type))
               (declare (ignore record))
               (setf (car heap-node) node))))
      ;; modify the edges as needed
      ;; note: if you are updating the edge, there should definitley be an element on the heap already
      ;; so we don't have to check if min-record exists.
      (let ((min-record (cl-heap:peep-at-heap cm-q)))
        (modify-edge g start-id end-id (get-bound-from-record min-record))))))

(declaim (inline get-bound-from-record))
(defun get-bound-from-record (record)
  (get-bound-from-constraint-type (first record) (second record)))

(declaim (inline get-bound-from-constraint-type))
(defun get-bound-from-constraint-type (constraint type)
  (case type
    (:ub (tn:upper-bound constraint))
    (:lb (- (tn:lower-bound constraint)))))
