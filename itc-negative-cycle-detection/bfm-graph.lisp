(in-package :itc-v2)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; This file implements Bellman Ford Moore Data Structures
;; as described in:
;; "Shortest Paths and Negative Cycle Detection
;; In Graphs with Negative Weights",
;; "I: The Bellman-Ford-Moore Algorithm Revisited."
;;  by Stefan Lewandowski, Universitat Stuttgart, FMI.
;; Technical Report No. 2010/05
;; August 3, 2010.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; TODO: REMOVE ORPHANED VERTICES (where the vertices are only connected from the vertex)
;;       Note that this is tricky because maybe the user has just added a vertex, in preparation
;;       for adding an edge?
;; TODO: Add in a save feature that allows the current state of the ITC to be snapshotted,
;;       so it can be restored.

(defparameter *a-then-b* :itc-ncd-a-then-b)

(defun enable-a-then-b (&optional (state t))
  (if state
      (pushnew *a-then-b* *features*)
      (setf *features* (remove *a-then-b* *features*))))

(defclass bfm-graph ()
  ((vertex-map
    :type t
    :initform nil
    :initarg :vertex-map
    :accessor vertex-map
    :documentation "A map from the ID of a vertex to its vertex object.")
   (start-vertex
    :type (or null bfm-vertex)
    :initform nil
    :initarg :start-vertex
    :accessor start-vertex
    :documentation "The starting vertex from which to do SSSP.")
   (set-a
    :type t
    :initform nil
    :initarg :set-a
    :accessor set-a
    :documentation "Set A")
   (set-b
    :type t
    :initform nil
    :initarg :set-b
    :accessor set-b
    :documentation "Set B")))

(defmethod initialize-instance :after ((g bfm-graph) ;; &rest rest ; BCW - rest is unused.  Removed to eliminate warning.
                                       &key (set-a-type :fifo) (set-b-type :fifo) (start-vertex-id nil) &allow-other-keys)
  (setf (start-vertex g) (if start-vertex-id (ensure-vertex g start-vertex-id) nil))
  (setf (set-a g) (make-queue :type set-a-type))
  (setf (set-b g) (make-queue :type set-b-type)))

(defmethod change-start-vertex ((g bfm-graph)(start-vertex-id t) &key (remove-incoming-edges nil) (remove-old-start-vertex nil))
  "Incrementally replaces the current start-vertex of the graph with a new start-vertex identified by START-VERTEX-ID.
   Returns two values, a boolean indicating whether the start has changed, and the start-vertex object.
   If REMOVE-INCOMING-EDGES is t, any existing in-bound edges on the new start-vertex will be removed.
   If REMOVE-INCOMING-EDGES is nil (default), nothing will change, and (nil <old-start-vertex>) will be returned.

   After using this method, one must still call (initialize) and (check-consistency) on graph G."
  (with-slots ((sv start-vertex)) g
    (let ((new-sv (if start-vertex-id (ensure-vertex g start-vertex-id) nil)))
      ;; check that we are using a new start-vertex
      (when (eq sv new-sv)
        (return-from change-start-vertex (values nil sv)))

      ;; cleanup the old start-vertex, if it existed.
      (when (and sv remove-old-start-vertex)
        ;; remove incoming edges to the start vertex
        (docoll ((in-vertices sv) in-vertex)
                (modify-edge g (id in-vertex) (id sv) +inf+))
        ;; remove outgoing edges from the start vertex
        (docoll ((out-edges sv) out-edge)
                (modify-edge g (id sv) (id (to-vertex out-edge)) +inf+))
        ;; remove the start vertex
        (map-remove! (vertex-map g) (id sv)))
      ;; remove incoming edges going to new start-vertex
      (when (and new-sv remove-incoming-edges)
        (docoll ((in-vertices new-sv) in-vertex)
                (modify-edge g (id in-vertex) start-vertex-id +inf+)))
      ;; set new start-vertex
      (setf sv new-sv)
      ;; return the new start-vertex
      (values t new-sv))))

(defun make-bfm-graph (&key (set-a-type :fifo) (set-b-type :fifo) (start-vertex-id nil) (id-equality-test 'eql))
  (make-instance 'bfm-graph :vertex-map (make-hashmap :test id-equality-test) :start-vertex-id start-vertex-id :set-a-type set-a-type :set-b-type set-b-type))

(defmethod print-object ((g bfm-graph) stream)
  (domap ((vertex-map g) v-id v-obj)
         (docoll ((out-edges v-obj) edge)
                 (format stream "~a->~a w:(~a)~%" v-id (id (to-vertex edge)) (w edge)))))

(defmethod debug-print-vertex-map ((g bfm-graph))
  (format t "VERTEX-MAP:~%")
  (domap ((vertex-map g) v-id v-obj)
         (declare (ignore v-id))
         (format t "~a~%" (debug-print-vertex-id v-obj nil))
         (format t "  left-sib: ~a~%" (debug-print-vertex-id (left-sib v-obj) nil))
         (format t "  right-sib: ~a~%" (debug-print-vertex-id (right-sib v-obj) nil))
         (format t "  child: ~a left:~a right:~a ~%"
                 (debug-print-vertex-id (child v-obj) nil)
                 (if (and (child v-obj) (left-sib (child v-obj)))
                     (debug-print-vertex-id (left-sib (child v-obj)) nil)
                     nil)
                 (if (and (child v-obj) (right-sib (child v-obj)))
                     (debug-print-vertex-id (right-sib (child v-obj)) nil)
                     nil))
         (format t "  parent: ~a ~%"
                 (debug-print-vertex-id (parent v-obj) nil))))

(defclass bfm-vertex ()
  ((id
    :type t
    :initform nil
    :initarg :id
    :accessor id
    :documentation "The ID of the vertex, typically a symbol or a number.")
   (out-edges
    :type hashset
    :initform (make-hashset :key-function #'to-vertex :test #'eql)
    :initarg :out-edges
    :accessor out-edges
    :documentation "A collection of directed edges of type bfm-edge that leave this vertex.")
   (;; in-edges is needed for negative cycle detection to re-queue nodes to be searched.
    in-vertices
    :type hashset
    :initform (make-hashset)
    :initarg :in-vertices
    :accessor in-vertices
    :documentation "A collection of bfm-vertex with an outgoing edge that enter this vertex.")
   (d
    :type number
    :initform +inf+
    :initarg :d
    :accessor d
    :documentation "The distance from the start to this node.")
   (parent
    :type (or null bfm-vertex)
    :initform nil
    :initarg :parent
    :accessor parent
    :documentation "The parent vertex of this vertex in the shortest path tree.")
   #+itc-ncd-a-then-b
   ;; scanning information (used when a-then-b of bfm-graph is true)
   (scanned
    :type number
    :initform nil
    :initarg :scanned
    :accessor scanned
    :documentation "a number, indiciating the most recent pass in which the node was scanned. 0 otherwise.")
   ;; set information for search
   (next-set-vertex
    :type (or null bfm-vertex)
    :initform nil
    :initarg :next-set-vertex
    :accessor next-set-vertex
    :documentation "The next vertex in this set.")
   (in-set
    :type symbol
    :initform nil
    :initarg :in-set
    :accessor in-set
    :documentation "T if this vertex is in set A or B. nil otherwise.")
   ;; subtree disassembly information
   (child
    :type (or null bfm-vertex)
    :initform nil
    :initarg :child
    :accessor child
    :documentation "One of the children of this vertex in the shortest path tree.")
   (left-sib
    :type (or null bfm-vertex)
    :initform nil
    :initarg :left-sib
    :accessor left-sib
    :documentation "A sibling of this vertex in the shortest path tree.")
   (right-sib
    :type (or null bfm-vertex)
    :initform nil
    :initarg :right-sib
    :accessor right-sib
    :documentation "A sibling of this vertex in the shortest path tree.")))

(defun make-bfm-vertex (&rest rest &key &allow-other-keys)
  ;; The data-structures out-edges and in-vertices in the class definition of bfm-vertex
  ;; do NOT need to be keyed off the ID of events because the supported algorithms
  ;; add and remove elements of the graph via a vertex object.
  (apply #'make-instance 'bfm-vertex rest))

(defmethod print-object ((v bfm-vertex) stream)
  (print-unreadable-object (v stream :type t :identity nil)
    (princ (id v) stream)))

(defmethod distance ((g bfm-graph)(id t))
  "Returns the distance from the start-vertex to vertex V.
   The value returned is only meaningful if the graph G is consistent and a start-vertex has been set.
   Actually returns two values:
   * If the vertex is found: <distance t>
   * If the vertex is NOT found: <+inf+ nil>"
  (let ((v (map-get (vertex-map g) id :default nil)))
    ;; (format t "~a~%" v)
    (if v
        (values (d v) t)
        (values +inf+ nil))))

(defmethod distance ((g bfm-graph)(v bfm-vertex))
  "Returns the distance from the start-vertex to vertex V.
   The value returned is only meaningful if the graph G is consistent and a start-vertex has been set."
  (d v))

(defmethod path ((g bfm-graph)(id t))
  "Returns a list of vertex objects from the start-vertex to V, including the start-vertex and V.
   The value returned is only meaningful if the graph G is consistent and a start-vertex has been set.
   Actually returns two values:
   * If the vertex is found: <distance t>
   * If the vertex is NOT found: <nil nil>"
  (let ((v (map-get (vertex-map g) id :default nil)))
    (if v
        (values (path g v) t)
        (values (list) nil))))

(defmethod path ((g bfm-graph)(v bfm-vertex))
  "Returns a list of vertex objects from the start-vertex to V, including the start-vertex and V.
   The value returned is only meaningful if the graph G is consistent and a start-vertex has been set."
  (let ((path (list)))
    (loop while v do
         (push v path)
         (setf v (parent v)))
    path))

(defclass bfm-edge ()
  ((id
    :type t
    :initform nil
    :initarg :id
    :accessor id
    :documentation "The ID of this edge, typically a symbol or number.")
   (w
    :type number
    :initform +inf+
    :initarg :w
    :accessor w
    :documentation "The weight of this edge.")
   (to-vertex
    :type (or null bfm-vertex)
    :initform nil
    :initarg :to-vertex
    :accessor to-vertex
    :documentation "The vertex to which this edge is pointing.")))

(defmethod find-vertex ((g bfm-graph)(id t) &key (default nil))
  "Finds a bfm-vertex object identified by ID.
   If the bfm-vertex object exists, it will be returned.
   If not, DEFAULT will be returned (which is nil, by default)."
  (map-get (vertex-map g) id :default default))

(defmethod ensure-vertex ((g bfm-graph)(id t))
  "Ensures a vertex with the given ID exists.
   Returns two values. The first value is a bfm-vertex object.
   The second value is t if a vertex was added. nil otherwise."
  (let ((v-obj (map-get (vertex-map g) id :default nil)))
    (if v-obj
        (values v-obj nil)
        (progn
          ;; make the vertex
          (setf v-obj (make-bfm-vertex :id id :d (if (start-vertex g) +inf+ 0)))
          ;; add new vertex to the vertex-map
          (map-add! (vertex-map g) id v-obj)
          ;; add an edge to connect the start-vertex to this vertex
          ;; (unless start-vertex
          ;;    (let ((start-edge (make-instance 'bfm-edge :w 0 :to-vertex v-obj)))
          ;;      (coll-add! (out-edges (start-vertex g)) start-edge)
          ;;      (coll-add! (in-vertices v-obj) (start-vertex g))))

          ;; return
          (values v-obj t)))))

(defmethod remove-vertex ((g bfm-graph)(v bfm-vertex))
  "Removes a vertex only if the vertex is no longer used by any edges.
   Returns t if the vertex was removed.
   Returns nil if the vertex is involved with an edge and NOT removed."
  (if (and (coll-empty? (out-edges v))
           (coll-empty? (in-vertices v)))
      (progn
        ;; remove the vertex from the set of verticies
        (map-remove! (vertex-map g) (id v))
        ;; remove the vertex from any Q.
        (setf (in-set v) nil)
        ;; return that the vertex was removed.
        t)
      ;; return that vertex was NOT removed.
      nil))

(defmethod find-edge ((g bfm-graph)(from-v-id t)(to-v-id t) &key (default nil))
  "Finds a bfm-edge object from a vertex FROM-V-ID to a vertex TO-V-ID.
   If the bfm-edge object exists, it will be returned.
   If not, DEFAULT will be returned (which is nil, by default)."
  (let ((from-v-obj (find-vertex g from-v-id))
        (to-v-obj (find-vertex g to-v-id)))
    (if (and from-v-obj to-v-obj)
        (coll-get (out-edges from-v-obj) to-v-obj :use-key-function nil :default default)
        nil)))

(defmethod ensure-edge ((g bfm-graph)(from-v-id t)(to-v-id t) &key (weight +inf+) (id nil))
  "Ensures an edge exists from a vertex FROM-V-ID to a vertex TO-V-ID.
   If the edge does NOT exist,
    the weight will be initialized to keyword paramter WEIGHT (default: infinity),
    the id will be initialized to keyword paramter ID.
   Returns two values. The first value is a bfm-edge object.
   The second value is t if an edge was added. nil otherwise."
  ;; coerce weight to be type double-float to fix floating-point error
  (setf weight (coerce weight 'double-float))
  (let* ((from-v-obj (ensure-vertex g from-v-id))
         (to-v-obj (ensure-vertex g to-v-id))
         (edge-obj (coll-get (out-edges from-v-obj) to-v-obj :use-key-function nil)))
    ;; decide what to do if the edge exists already or not.
    (if edge-obj
        ;; if the edge-object exists
        (values edge-obj nil)
        ;; if the edge-object does not exist
        (progn
          (setf edge-obj (make-instance 'bfm-edge :id id :w weight :to-vertex to-v-obj))
          (coll-add! (out-edges from-v-obj) edge-obj)
          (coll-add! (in-vertices to-v-obj) from-v-obj)
          ;; return
          (values edge-obj t)))))

(defmethod modify-edge ((g bfm-graph)(from-v-id t)(to-v-id t)(new-weight number))
  "Changes the weight of a directed edge that starts at FROM-V-ID and ends at TO-V-ID
   to NEW-WEIGHT. Where FROM-V-ID and TO-V-ID are vertex ids, not vertex objects.
   If NEW-WEIGHT is positive infinity (+inf+),
     the edge is removed from the graph.
   Otherwise, the edge is created if it does not exist.
   Returns the modified/created/removed edge-object."
  ;; coerce weight to be type double-float to fix floating-point error
  (setf new-weight (coerce new-weight 'double-float))
  ;; check vertices to be requeued.
  (let* ((edge-obj (ensure-edge g from-v-id to-v-id :weight new-weight))
         ;; (edge-weight (w edge-obj))
         (from-v-obj (ensure-vertex g from-v-id))
         (to-v-obj (to-vertex edge-obj)))
    ;; set the cost to the edge
    (setf (w edge-obj) new-weight)

    ;; (format t "~a -> ~a : ~a~%" from-v-id to-v-id new-weight)
    ;; (format t "~a ~a ~a ~%" (d from-v-obj) new-weight (d to-v-obj))

    ;; Decide how to incrementally fix
    (cond (;; Change improves shortest path
           (> (- (d to-v-obj) double-float-epsilon) (+ (d from-v-obj) new-weight))
           ;; Update and propagate
           (setf (d to-v-obj) (+ (d from-v-obj) new-weight))
           (delete-subtree to-v-obj)
           (set-parent from-v-obj to-v-obj)
           ;; (setf (parent to-v-obj) from-v-obj)
           (enqueue! (set-b g) to-v-obj))
          (;; Change worsens shortest path
           (and (< (+ (d to-v-obj) double-float-epsilon) (+ (d from-v-obj) new-weight))
                (eq (parent to-v-obj) from-v-obj))
           ;; Reset value
           (let ((parent-map (delete-subtree to-v-obj)))
             ;; ;; add to-v-obj to map
             ;; (map-add! parent-map from-v-obj to-v-obj)
             ;; ;; requeue the from-obj
             ;; (enqueue! (set-b g) from-v-obj)
             ;; requeue the children for re-support
             (insert-parents g parent-map))))
    ;; Check if any vertices need to be supported from the start-vertex
    ;; this code assumes we will never be able to call modify-edge on
    ;; an edge involving the graph's start-vertex.
    (when (= new-weight +inf+)
      ;; remove the edge
      (coll-remove! (out-edges from-v-obj) edge-obj)
      (coll-remove! (in-vertices to-v-obj) from-v-obj)
      ;; remove the from-vertex if there are no edges using it.
      (remove-vertex g from-v-obj)
      ;; remove the to-vertex if there are no edges using it.
      (remove-vertex g to-v-obj))
    edge-obj))

(defun create-edge-id (from-v-id to-v-id)
  (format nil "~a->~a" from-v-id to-v-id))
