(in-package :itc-v2)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; This file implements an extenson to the
;; Bellman Ford Moore algorithm
;;  described in:
;; "Shortest Paths and Negative Cycle Detection
;; In Graphs with Negative Weights",
;; "I: The Bellman-Ford-Moore Algorithm Revisited."
;;  by Stefan Lewandowski, Universitat Stuttgart, FMI.
;; Technical Report No. 2010/05
;; August 3, 2010.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; BELLMAN, FORD, MOORE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod set-a-to-b ((g bfm-graph))
  (let* ((temp-a (set-a g))
         (type-a (queue-type temp-a)))
    ;; switch types first
    (setf (queue-type (set-a g)) (queue-type (set-b g)))
    (setf (queue-type (set-b g)) type-a)
    ;; switch queues
    (setf (set-a g) (set-b g))
    (setf (set-b g) temp-a)))

(defmethod set-b-to-a ((g bfm-graph))
  (let* ((temp-b (set-b g))
         (type-b (queue-type temp-b)))
    ;; switch types first
    (setf (queue-type (set-b g)) (queue-type (set-a g)))
    (setf (queue-type (set-a g)) type-b)
    ;; switch queues
    (setf (set-b g) (set-a g))
    (setf (set-a g) temp-b)))

(defmethod add-a-to-b ((g bfm-graph))
  "Adds all the elements of queue a to queue b."
  (loop until (isempty? (set-a g)) do
       (enqueue! (set-b g) (dequeue! (set-a g)))))

(defmethod initialize ((g bfm-graph))
  (log:debug "INITIALIZE ITC")
  ;; empty the Q's
  (empty! (set-a g))
  (empty! (set-b g))
  ;; reset the vertices
  (if (start-vertex g)
      (progn
        (domap ((vertex-map g) v-id v-obj)
               (declare (ignore v-id))
               (setf (d v-obj) (coerce +inf+ 'double-float)) ;; floating-point error fix
               (setf (parent v-obj) nil)
               (setf (child v-obj) nil)
               (setf (left-sib v-obj) nil)
               (setf (right-sib v-obj) nil))
        (setf (d (start-vertex g)) (coerce 0 'double-float)) ;; floating-point error fix
        ;; enqueue start-vertex
        (enqueue! (set-b g) (start-vertex g)))
      (domap ((vertex-map g) v-id v-obj)
             (declare (ignore v-id))
             (setf (d v-obj) (coerce 0 'double-float)) ;; floating-point error fix
             (setf (parent v-obj) nil)
             (setf (child v-obj) nil)
             (setf (left-sib v-obj) nil)
             (setf (right-sib v-obj) nil)
             (enqueue! (set-b g) v-obj)))
  (log:debug "A: ~a" (debug-print (set-a g) nil))
  (log:debug "B: ~a" (debug-print (set-b g) nil)))

(defmethod check-consistency ((g bfm-graph))
  (log:debug "CHECK-CONSISTENCY")

  (let (#+itc-ncd-a-then-b
        (pass 0)))
  #+itc-ncd-a-then-b
  (domap ((vertex-map g) v-id v-obj)
         (declare (ignore v-id))
         (setf (scanned v-obj) 0))
  ;; 10. repeat...until B==empty_set

  (log:debug "Initial Set-B: ~a~%" (set-b g))
  (loop until (isempty? (set-b g)) do
     ;; 11. A:=B; B:=empty_set
       (set-a-to-b g)
       (empty! (set-b g))

       (log:debug "LOOP UNTIL B IS EMPTY")
       (log:debug "SETTING A = B")
       (log:debug "A: ~a" (debug-print (set-a g) nil))
       (log:debug "B: ~a" (debug-print (set-b g) nil))

       ;; 12. repeat... until A==empty_set
       #+itc-ncd-a-then-b
       (incf pass)
       (loop until (isempty? (set-a g)) do
            (log:debug "  LOOP UNTIL A IS EMPTY")
            (log:debug "A: ~a" (debug-print (set-a g) nil))

          ;; 13. u:=getElement(A); A:=A\{u}; // remove an arbitrary node
            (let ((u (dequeue! (set-a g)))
                  v)

              ;; negative cycle detection: re-assert the u is in set a.
              (setf (in-set u) t)
              (log:debug "    ASSERT U IS IN SET: ~a" (debug-print-vertex-id u nil))

              ;; 14. for all edges (u,v) elem-of E // and "scan it"
              #+itc-ncd-a-then-b
              (setf (scanned u) pass)

              (docoll ((out-edges u) edge)
                      (setf v (to-vertex edge))
                      (log:debug "        U: ~a (~a)  V: ~a (~a)"
                                 (debug-print-vertex-id u nil) (d u)
                                 (debug-print-vertex-id v nil) (d v))

                      ;; 15. if D(u) + gamma(u,v) < D(v) then
                      (when (< (+ (d u) (w edge)) (- (d v) double-float-epsilon))
                        (log:debug "        (~a + ~a) < ~a" (d u) (w edge) (d v))

                        ;; negative cycle detection:
                        ;;   - subtree-disassembly on all of v's children
                        ;;   - then detect if u is in v's subtree
                        (let ((parent-map (delete-subtree v))
                              neg-cycle)
                          (when (not (in-set u))
                            (log:debug "          when (not (in-set u))")
                            ;; negative cycle detected, extract negative cycle
                            (setf neg-cycle (extract-negative-cycle parent-map u v))

                            ;; THE ORIGINAL ALGORITHM:
                            ;; reset b for the next run.
                            ;; (empty! (set-b g))
                            ;; (reset-from-negative-cycle g neg-cycle parent-map)

                            ;; UPDATED ALGORITHM FOR SUBTREE DISASSEMBLY
                            ;; requeue everything on a
                            ;; enqueue u onto b
                            ;; TODO: do we need to copy remaining nodes from a onto b?
                            ;; add nodes from a onto b
                            (add-a-to-b g)
                            (enqueue! (set-b g) u)

                            (log:debug "A: ~a" (debug-print (set-a g) nil))
                            (log:debug "B: ~a" (debug-print (set-b g) nil))
                            ;; (debug-print-vertex-map g)
                            ;; return the negative cycle
                            (log:debug "          Negative Cycle: ~a" neg-cycle)
                            (return-from check-consistency (values nil neg-cycle))))

                        ;; 16. D(v):=D(u)+gamma(u,v);
                        (setf (d v) (+ (d u) (w edge)))

                        ;; 16.  parent(v):=u
                        (log:debug "        SET PARENT OF ~a TO ~a"
                                   (debug-print-vertex-id v nil)
                                   (debug-print-vertex-id u nil))
                        (set-parent u v)

                        ;; DECIDE WHETHER TO ADD TO A OR B FIRST
                        #-itc-ncd-a-then-b
                        ;; 17. if v not-elem A union B
                        (when (not (in-set v))
                          ;; 18. B:=B union {v};
                          (log:debug "        ENQUEUE! (set-b g) ~a"  (debug-print-vertex-id v nil))
                          (enqueue! (set-b g) v))
                        #+itc-ncd-a-then-b
                        ;; if v not-elem A union B
                        (when (not (in-set v))
                          ;; if not scanned(v) then
                          (if (not (eq (scanned v) pass))
                              ;; A:=A union {v};
                              (progn
                                (log:debug "        ENQUEUE! (set-a g) ~a"  (debug-print-vertex-id v nil))
                                (enqueue! (set-a g) v))
                              ;; B:=B union {v};
                              (progn
                                (log:debug "        ENQUEUE! (set-b g) ~a"  (debug-print-vertex-id v nil))
                                (enqueue! (set-b g) v))))))

              ;; negative cycle detection: un-assert the u is in set a.
              (setf (in-set u) nil)

              (log:debug "    AFTER SCANNING OUTBOUND EDGES"))))
  (values t nil))

(defmethod reset-from-negative-cycle ((g bfm-graph)(negative-cycle list) (parent-map list))
  ;; Reset value
  (dolist (vertex-id-pair negative-cycle)
    (let* ((c (map-get (vertex-map g) (second vertex-id-pair))))
      ;; requeue the children for re-support
      (insert-parent g c))))

(defmethod insert-parent ((g bfm-graph)(c bfm-vertex))
  ;; (format t "insert-parents: ~a ~%" (debug-print-vertex-id c nil))
  ;; Reset the vertex (but do NOT reset the next-set-vertex pointer)
  (if (null (start-vertex g))
      ;; if there is a dummy-start-vertex (start-vertex=nil) this child should be distance 0 from start node.
      (setf (d c) 0)
      ;; if there is a start-vertex, set this child to infinity and parent to nil.
      (progn
        (setf (d c) +inf+)
        (setf (parent c) nil)))

  ;; enqueue all of the predecessors of c (all of the incoming edges).
  (docoll ((in-vertices c) pred)
          ;; Only enqueue when distance is not infinity or parent is known
          ;; The logic of this check is, you want to find the 'blanket'
          ;; of all precessor vertices in the graph that could be the next
          ;; parent to this vertex c.
          ;; If the parent has distance +inf+, it is no help in shortening the path to c.
          ;; If the parent itself has no parent, then it is either the start-vertex, or
          ;;   is already in the Q.
          ;; Note: The start vertex has distance 0, so it will be enqueued.
          (when (or (not (= (d pred) +inf+)) (parent pred))
            ;; (format t "INSERT-PARENTS, enqueing: ~a~%" (debug-print-vertex-id pred))
            (enqueue! (set-b g) pred))))

(defmethod insert-parents ((g bfm-graph) (parent-map list))
  ;; (domap (parent-map c p)
  ;;     (declare (ignore p))
  ;;     (insert-parent g c))
  (loop for c-p in parent-map do
     ;;(declare (ignore p))
       (insert-parent g (first c-p))))

(defmethod extract-negative-cycle ((parent-map list) (u bfm-vertex) (v bfm-vertex))
  "Returns the negative cycle as a list of vertex-id pairs. i.e.
   ((a b) (b c) (c a))"

  ;; (format t "parent-map:~%")
  ;; (domap (parent-map c p)
  ;;         (format t "~a -> ~a~%" (id c) (id p)))
  ;; (format t "u: ~a~%" (id u))
  ;; (format t "v: ~a~%" (id v))

  (let ((cycle (make-smart-list))
        (c u)
        (p nil))
    ;; add edge from u to v
    (coll-add! cycle `(,(id u) ,(id v)))
    ;; add edges from the parent-map (computed from subtree disassembly)
    (loop while (not (eq c v)) do
         ;;(setf p (map-get parent-map c))
         (setf p (cdr (assoc c parent-map)))
         (when (not p)
           (setf p (parent c)))
         (coll-add! cycle `(,(id p) ,(id c)))
         (setf c p))

    ;; (format t "~a~%" (reverse (coll-tolist cycle)))

    (reverse (coll-tolist cycle))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SUBTREE DISASSEMBLY
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod set-parent ((p bfm-vertex) (c bfm-vertex))
  "This function both sets P to be the parent of C
   and C to be the child of P (maintains the CLL of children)."
  ;; (format t "SET-PARENT: ~a ~a~%" (debug-print-vertex-id p nil) (debug-print-vertex-id c nil))

  ;; This method assumes that child has already been removed from its parent
  ;; in a manner that maintains the subtree.
  (assert (or (eq (parent c) nil)
              (eq (parent c) p)))

  (setf (parent c) p)
  (set-child p c))

(defmethod set-child ((p bfm-vertex) (c bfm-vertex))
  "Set child ONLY sets C to be the child of P
   (and maintains the Circularly Linked List of children of P).
   It does NOT set P to the parent of C.
   This was done to create a clear abstraction-divide between
   the subtree representation stored with each vertex,
   and the standard parent pointer also stored with each vertex."
  ;; (format t "SET-CHILD: ~a ~a~%" (debug-print-vertex-id p nil) (debug-print-vertex-id c nil))
  ;; (format t "~a~%" (debug-print-vertex-id p nil))
  ;; (format t "left-sib: ~a~%" (left-sib p))
  ;; (format t "right-sib: ~a~%" (right-sib p))
  ;; (format t "~a~%" (debug-print-vertex-id c nil))
  ;; (format t "left-sib: ~a~%" (left-sib c))
  ;; (format t "right-sib: ~a~%" (right-sib c))

  (let ((old-c (child p)))

    ;;If there is an old child node, check how we should add the new child.
    (if old-c
        ;; If the old-child is NOT the same as the new child, add it.
        (unless (eq old-c c)
          (setf (right-sib c) (right-sib old-c))
          (setf (right-sib old-c) c)
          (setf (left-sib c) old-c)
          (setf (left-sib (right-sib c)) c))
        ;; If there is no child node... add the new child.
        (progn
          (setf (child p) c)
          (setf (right-sib c) c)
          (setf (left-sib c) c)))))

(defmethod debug-is-child ((p bfm-vertex) (c bfm-vertex))
  "This method checks ONLY whether C is a child of P.
   It does NOT check whether P is the parent of C.
   This method was designed to check the structure of the CLL,
   and is therefore not as efficient as a bare-bones implementation.
   Returns three values:
    1. t if the child was found traversing the CLL left and right.
    2. t if the child was found traversing the CLL right-sib pointers.
    3. t if the child was found traversing the CLL left-sub pointers."
  (let ((curr (child p))
        (found-going-right nil)
        (found-going-left nil))
    ;; find the child traversing right
    (loop while curr do
         (when (eq curr c)
           (setf found-going-right t)
           (return))
         ;;;; go to the next
         (setf curr (right-sib curr))
         ;;;; stop if we've looped around
         (if (eq curr (child p)) (return)))
    ;; find the child traversing left
    (loop while curr do
         (when (eq curr c)
           (setf found-going-left t)
           (return))
         ;;;; go to the next
         (setf curr (right-sib curr))
         ;;;; stop if we've looped around
         (if (eq curr (child p)) (return)))
    (values (and found-going-right found-going-left)
            found-going-right
            found-going-left)))

(defmethod delete-subtree ((v bfm-vertex) &optional (parent-map (list)))
  "Deletes V and all children in the shortest-path tree.
  This method will check if V is a bfm-vertex and is (in-set V)
  before deleting."
  ;; (format t "DELETE-SUBTREE ~a~%" (debug-print-vertex-id v nil))
  ;; (format t "~a~%" (debug-print-vertex-id v nil))

  (when (and v (not (parent v)))
    (return-from delete-subtree parent-map))

  ;; if (v neq null)
  (let ((parent-v (parent v)))
    ;; LAZILY remove v from sets A and B;
    (setf (in-set v) nil)
    ;; negative-cycle: record the parent so the negative cycle can be recovered
    ;;(map-add! parent-map v parent-v)
    (setf parent-map (acons v parent-v parent-map))
    ;; remove parent pointer - breaks abstraction, separation of subtree disassembly fields from standard vertex fields.
    (setf (parent v) nil)
    ;; for all children v_c
    (let* ((vc (child v))
           next-vc)
      (loop while vc do
         ;; record the next vc
           (setf next-vc (right-sib vc))
         ;; delete_a_subtree(v_c);
           (setf parent-map (delete-subtree vc parent-map))
           ;;;; stop if we've looped around
           (if (eq vc next-vc) (return))
           ;;;; go to the next
           (setf vc next-vc)))

      ;; remove v from shortest path tree
      ;;;; remove from circularly linked list
      (cond
        ;; (;; this vertex is not the child of anything.
        ;;   (eq (parent v) nil)
        ;;   ;; do nothing - it is already not in a subtree
        ;;   )
        (;; this is the only child.
         (eq (right-sib v) v)
         ;; if the cll has only one element
         ;; if this is the only child of the parent, remove child from parent
         (if (eq (child parent-v) v)
             (setf (child parent-v) nil)))
        (;; if the cll has more than one element
         ;; move to the right
         :otherwise
         (progn
           (setf (right-sib (left-sib v)) (right-sib v))
           (setf (left-sib (right-sib v)) (left-sib v))
           ;; if this is the first child of the parent, remove child from parent
           (if (eq (child parent-v) v)
               (setf (child parent-v) (right-sib v))))))

      ;;;; set its own left and right pointers to nil
      (setf (right-sib v) nil)
      (setf (left-sib v) nil))
  parent-map)

;; (defmethod delete-child-subtrees ((g bfm-graph) (v bfm-vertex) &optional (parent-map (make-hashmap)))
;;   ;; (format t "DELETE-CHILD-SUBTREES~%")
;;   ;; (format t "~a~%" (debug-print-vertex-id v nil))

;;   (debug-print-vertex-map g)
;;   (let* ((vc (child v))
;;         next-vc)
;;  (loop while vc do
;;     ;; record the next vc
;;       (setf next-vc (right-sib vc))
;;     ;; add this child to parent-map
;;       (map-add! parent-map vc v)
;;     ;; delete_a_subtree(v_c)
;;       (delete-subtree vc parent-map)
;;       ;;;; stop if we've looped around
;;       (if (eq vc next-vc) (return))
;;       ;;;; go to the next
;;       (setf vc next-vc)
;;       ))
;;   ;; remove the child from the parent
;;   (setf (child v) nil)
;;   (debug-print-vertex-map g)
;;   parent-map)

(defmethod debug-print-subtree ((v bfm-vertex) &key (stream t) (offset ""))
  (with-output-to-format stream
    ;; print all the children of this vertex
    (format stream "~a" offset)
    (debug-print-cll (child v) stream)
    (format stream "~%")
    ;; print the children of those children
    (let ((curr (child v)))
      ;; find the child traversing right
      (loop while curr do
           (debug-print-subtree curr :stream stream :offset (format nil "~a " offset))
         ;; go to the next
           (setf curr (right-sib curr))
         ;; stop if we've looped around
           (if (eq curr (child v)) (return))))))

(defmethod debug-print-cll ((v bfm-vertex) &optional (stream t))
  "Prints the circularly linked list of vertices in the form:
   (P->C)P=>(<->A<->B*<->C<->)
   *Everything to the left of => represents the parents of the CLL.
   *Everything to the right of the => represents the children of CLL.
   *In the parent representation, the parent of the vertex V on which
   this function was called is printed. Any other vertices with
   parents that are NOT the same as V will be printed before as (P->C).
   *In the CLL representation, the * represents a lazy delete,
   the <-> represents whether the elements have a left and right pointer."
  (with-output-to-format stream
    ;; check that all vertices have the same parent
    (let ((curr v)
          (parent (parent v)))
      (loop while curr do
           (unless (eq (parent curr) parent)
             (format stream "(~a->~a)"
                     (debug-print-vertex-id (parent curr) nil)
                     (debug-print-vertex-id curr nil) ))
         ;; go to the next
           (setf curr (right-sib curr))
         ;; stop if we've looped around
           (if (eq curr v) (return)))
      (if parent
          (format stream "~a=>" (debug-print-vertex-id parent nil))
          (format stream "nil=>")))
    (format stream "(")
    (let ((curr v))
      (loop while curr do
           (let ((l nil)
                 (lr nil)
                 (r nil)
                 (rl nil))
             (when (left-sib v)
               (setf l t)
               (when (eq (right-sib (left-sib v)) v)
                 (setf lr t)))
             (when (right-sib v)
               (setf r t)
               (when (eq (left-sib (right-sib v)) v)
                 (setf rl t)))
             ;; print left right pointers
             (format stream "~a-~a" (if l "<" "") (if lr ">" ""))
             ;; print id
             (debug-print-vertex-id curr stream)
             ;; print left right pointers
             (when (eq (right-sib curr) v)
               (format stream "~a-~a" (if rl "<" "") (if r ">" ""))))
         ;; go to the next
           (setf curr (right-sib curr))
         ;; stop if we've looped around
           (if (eq curr v) (return)))
      (format stream ")"))))

(defun debug-print-vertex-id (v &optional (stream t))
  (with-output-to-format stream
    (if v
        (progn
          ;; print id
          (format stream "~a" (id v))
          ;; mark for lazily-removed
          (unless (in-set v)
            (format stream "*")))
        (format stream "nil"))))
