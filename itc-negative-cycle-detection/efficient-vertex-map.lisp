;;;; Copyright (c) 2011 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; Authors:
;;;;   David Wang

(in-package #:itc-v2)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; HASHMAP
;; A wrapper for a hash-table, to provide a consistent interface.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass efficient-vertex-map (hashmap)
  ((simple-vertices
    :type doubly-linked-list
    :initform (doubly-linked-list)
    :initarg :simple-vertices
    :accessor evm-simple-vertices
    :documentation "A list of simple-vertices, for looping over.")))

(defmethod initialize-instance :after ((map efficient-vertex-map))
  (with-slots (simple-vertices) map))

(defun make-efficient-vertex-map (&rest rest &key &allow-other-keys)
  (apply #'make-instance 'efficient-vertex-map rest))

(defmethod-alias evm-add! map-add! ((map efficient-vertex-map)(key t)(value t) &key (replace t)(default nil) &allow-other-keys)
  "Adds the KEY VALUE pair to the MAP.
   Returns two values. The first value is boolean and represents if the KEY VALUE
   pair was added. The second value is the original value associated with the KEY,
   DEFAULT otherwise.
   If REPLACE is t (default), the new key-value pair will replace the old key-value pair."
  (let (added? old-value)
    (if (typep bfm-vertex)
        (let (node (make-instance 'dll-node :element value))
          (multiple-value-setq (added? old-value)
            (call-next-method map key node :replace replace :default default))
          (when added?
            (dll-add-node! (evm-simple-vertices map) node)))
        (multiple-value-setq (added? old-value)
          (call-next-method))))
  ;; make sure we don't return a doubly-linked-list node
  (if (typep old-value 'dll-node)
      (values added? (dll-node-element old-value))
      (values added? old-value)))

(defmethod-alias hm-remove! map-remove! ((map hashmap)(key t) &key (default nil) &allow-other-keys)
  "Removes the KEY and associated value from the MAP.
   Returns two values.
     If the KEY is found, the first value will be the value
   associated with the key, the second value will be t,
   indicating something was removed.
     If the KEY is not found, the first value will be DEFAULT
   the second value will be nil."
  (multiple-value-bind (old-value removed?)
      (call-next-method)
    (if (typep old-value 'dll-node)
        (dll-remove-node! (evm-simple-vertices map) old-value))))

(defmethod-alias hm-get map-get ((map hashmap)(key t) &key (default nil) &allow-other-keys)
  "Returns the value associated with KEY in the MAP.
   Returns two values.
     If the KEY is found, the first value will be the value
   associated with the key, the second value will be t,
   indicating the key was found.
     If the KEY is not found, the first value will be DEFAULT
   the second value will be nil."
  (gethash key (hm-table map) default))

(defmethod-alias hm-clear! map-clear! ((map hashmap) &key &allow-other-keys)
  "Clears the MAP of all contents."
  ;; clear the table
  (clrhash (hm-table map)))

(defmethod-alias hm-contains? map-contains? ((map hashmap)(key t) &rest rest &key &allow-other-keys)
  "Returns t if the MAP contains the KEY
   and nil if it does not."
  (apply #'hm-contains-key? map key rest))

(defmethod-alias hm-contains-key? map-contains-key? ((map hashmap)(key t) &key &allow-other-keys)
  "Returns t if the MAP contains the KEY
   and nil if it does not."
  (multiple-value-bind (orig-value there?)
      (gethash key (hm-table map))
    (declare (ignore orig-value))
    there?))

(defmethod-alias hm-contains-value? map-contains-value? ((map hashmap)(value t) &key (test #'eql)(default nil) &allow-other-keys)
  "Returns two values.
   The first value is t if the MAP contains the VALUE, DEFAULT otherwise.
   The second value is the key associated with the VALUE."
  (block stop-mapping
    (hm-map map
            (lambda (k v)
              (when (funcall test v value)
                (return-from stop-mapping (values t k))
                (return-from stop-mapping (values nil default)))))))

(defmethod-alias hm-size map-size ((map hashmap) &key &allow-other-keys)
  "Returns the number of KEY VALUE pairs stored in the MAP."
  (hash-table-count (hm-table map)))

(defmethod-alias hm-empty? map-empty? ((map hashmap) &key &allow-other-keys)
  "Returns t if the MAP contains no keys, nil otherwise."
  (eql (map-size map) 0))

(defmethod-alias hm-map map-map ((map hashmap) (f function) &key &allow-other-keys)
  "Applies a function F to each entry in the MAP.
   The function is required to take two arguments - the key and the value."
  (maphash f (hm-table map)))

(defmethod print-object ((map hashmap) stream)
  (let ((first-loop t))
    (hm-map map
             (lambda (key value)
               (unless first-loop
                 (format stream ","))
               (format stream "~a:~a" key value)
               (setf first-loop nil)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ITERATOR
;; Not supported by hashmap class.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod make-map-iterator ((map hashmap) &key &allow-other-keys)
  "Creates an iterator object that maintains its state over the MAP.
  This method should throw an error if it is not supported."
  (error 'unsupported-function))




(check-implementation 'hashmap)
