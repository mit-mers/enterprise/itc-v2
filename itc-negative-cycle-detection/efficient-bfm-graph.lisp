(in-package :itc-v2)

;; (defclass efficient-bfm-graph (bfm-graph)
;;   ((vertex-count-map
;;     :initform (make-hashmap :test #'eql)
;;     :type hashmap
;;     :initarg :vertex-count-map
;;     :accessor vertex-count-map
;;     :documentation "A map from the symbolic name of a vertex to an integer, the number of connected undirected edges.")
;;    (processed-root-set
;;     :initform (make-hashset :test #'eql)
;;     :type hashset
;;     :initarg :processed-root-set
;;     :accessor processed-root-set
;;     :documentation "")
;;    (unprocessed-root-set
;;     :initform (make-hashset :test #'eql)
;;     :type hashset
;;     :initarg :unprocessed-root-set
;;     :accessor unprocessed-root-set
;;     :documentation "")

;;    ;; (efficient-vertex-map
;;    ;;  :initform (make-hashmultimap :test #:eql)
;;    ;;  :type hashmultimap
;;    ;;  :initarg :efficient-vertex-map
;;    ;;  :accessor efficient-vertex-map
;;    ;;  :documentation "A map from the name of a vertex to the edge connected to it."
;;    ;;  )
;;    ;; (efficient-edge-map
;;    ;;  :type hashmap
;;    ;;  :initform (make-hashmap :test #'equal)
;;    ;;  :initarg :efficient-edge-map
;;    ;;  :accessor efficient-edge-map
;;    ;;  :documentation "A map from "
;;    ;;  )
;;    )
;;   )

(defclass ebfm-graph (bfm-graph)
  ())
 ;; (vertex-map
  ;;  :type t
  ;;  :initform nil
  ;;  :initarg :vertex-map
  ;;  :accessor vertex-map
  ;;  :documentation "A map from the name of a vertex to its vertex object.")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; EBFM VERTEX CLASS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass ebfm-vertex ()
  (name
   :type t
   :initform nil
   :initarg :name
   :accessor name
   :documentation "The name of the vertex, typically a symbol or a number.")
  (active-edges
   :type hashset
   :initform (make-hashset :test #'eql)
   :initarg :out-edges
   :accessor out-edges
   :documentation "A collection of UNdirected edges of type bfm-edge that leave this vertex.")
  (inactive-edges
   :type hashset
   :initform (make-hashset :test #'eql)
   :initarg :out-edges
   :accessor out-edges
   :documentation "A collection of UNdirected edges of type bfm-edge that leave this vertex."))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; BASE EDGE CLASS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass ebfm-edge ()
  ((vertex1
    :type symbol
    :initform nil
    :initarg :vertex1
    :accessor vertex1
    :documentation "The 'start' vertex of this edge.")
   (vertex2
    :type symbol
    :initform nil
    :initarg :vertex2
    :accessor vertex2
    :documentation "The 'end' vertex of this edge.")
   (active
    :type t
    :initform t
    :accessor active
    :accessor active
    :documentation "Whether this edge is actively participating in the graph.")
   (parent-edge
    :type ebfm-edge
    :initform nil
    :initarg :parent-edge
    :accessor parent-edge
    :documentation "The parent edge object of this edge.")
   (parent-item
    :type t
    :initform nil
    :initarg :parent-item
    :accessor parent-item
    :documentation "Item that helps remove this edge from its parent data structure.")))

(defmethod print-object ((edge ebfm-edge)(stream stream))
  (print-unreadable-object (edge stream :type t :identity nil)
    (format stream "VERT:~a->~a" (vertex1 edge) (vertex2 edge))))

(defmethod get-weight ((edge ebfm-edge)(from-v symbol)(to-v symbol))
  (with-slots (vertex1 vertex2) edge
    (cond ((and (eql from-v vertex1) (eql to-v vertex2))
           (get-fwd-weight edge))
          ((and (eql from-v vertex2) (eql to-v vertex1))
           (get-bwd-weight edge))
          (:otherwise
           (error "Cannot get the weight of an edge.")))))

(defmethod get-fwd-weight ((edge ebfm-edge))
  (error "unsupported"))

(defmethod get-bwd-weight ((edge ebfm-edge))
  (error "unsupported"))

(declaim (inline set-parent-edge))
(defmethod set-parent-edge ((edge ebfm-edge)(parent-edge null) &optional (parent-item nil))
  (setf (parent-edge edge) parent-edge)
  (setf (parent-item edge) parent-item))

(defmethod set-parent-edge ((edge ebfm-edge)(parent-edge ebfm-edge) &optional (parent-item nil))
  (setf (parent-edge edge) parent-edge)
  (setf (parent-item edge) parent-item))

(defmethod start-vertex? ((edge ebfm-edge) (vertex symbol))
           (eql (vertex1 edge) vertex))

(defmethod end-vertex? ((edge ebfm-edge) (vertex symbol))
           (eql (vertex2 edge) vertex))

(defmethod member-vertex? ((edge ebfm-edge) (vertex symbol))
  (or (eql (vertex1 edge) vertex) (eql (vertex2 edge) vertex)))

(defmethod member-vertices ((edge ebfm-edge))
  (list (vertex1 edge) (vertex2 edge)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; BASE CLASSES FOR PARALLEL AND SEQUENTIAL EDGES
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass ebfm-par-edge (ebfm-edge)
  ())

(defclass ebfm-seq-edge (ebfm-edge)
  ())

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; DIRECTED EDGE CLASS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass ebfm-dir-edge (ebfm-edge)
  ((weight
    :type t
    :initform nil
    :initarg :weight
    :accessor weight
    :documentation "Weight of the directed edge")))

(defmethod print-object ((edge ebfm-dir-edge)(stream stream))
  (print-unreadable-object (edge stream :type t :identity nil)
    (format stream "VERT:~a->~a W:~a" (vertex1 edge) (vertex2 edge) (weight edge))))

(defmethod get-fwd-weight ((edge ebfm-dir-edge))
  (weight edge))

(defmethod get-bwd-weight ((edge ebfm-dir-edge))
  +inf+)

(defmethod equal-dir-edge ((edge1 ebfm-edge) (edge2 ebfm-edge))
  (and (typep edge1 'ebfm-dir-edge)
       (typep edge2 'ebfm-dir-edge)
       (eql (vertex1 edge1) (vertex1 edge2))
       (eql (vertex2 edge1) (vertex2 edge2))
       (= (weight edge1) (weight edge2))))

(defmethod member-edge? ((edge ebfm-dir-edge)(child-edge ebfm-edge) &key (depth 1) (equality #'eql) &allow-other-keys)
  (assert (or (null equality) (functionp equality)))
  (assert (numberp depth))
  (if (> depth 0)
      (if equality
          (funcall equality edge child-edge)
          nil)
      nil))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; EDGE RECORD
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defstruct edge-record
  (dir) ;; t = forward (vertex1->vertex2), nil = backward (vertex2->vertex1)
  (edge));; an edge object

(declaim (inline edge-record-weight))
(defmethod edge-record-weight ((record edge-record) &key (reverse nil))
  (with-slots (dir edge) record
    (if reverse
        (if dir (get-bwd-weight edge) (get-fwd-weight edge))
        (if dir (get-fwd-weight edge) (get-bwd-weight edge)))))

(defmethod print-object ((record edge-record)(stream stream))
  (with-slots (dir edge) record
  (let ((arrow (typecase edge
                 (ebfm-dir-edge (if dir "->" "<-"))
                 (ebfm-par-edge (if dir "|>" "<|"))
                 (ebfm-seq-edge (if dir "=>" "<=")))))
    (if dir
        (format stream "[~a~a~a]" (vertex1 edge) arrow (vertex2 edge))
        (format stream "[~a~a~a]" (vertex2 edge) arrow (vertex1 edge))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LIST-BASED PARALLEL EDGE CLASS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass ebfm-par-edge-list (ebfm-par-edge)
  ((fwd-edge-records
    :type list
    :initform (list)
    :initarg :fwd-edge-records
    :accessor fwd-edge-records
    :documentation "Set of all of the edges going forward.")
   (bwd-edge-records
    :type list
    :initform (list)
    :initarg :bwd-edge-records
    :accessor bwd-edge-records
    :documentation "Set of all of the edges going backward.")))

(defmethod print-object ((edge ebfm-par-edge-list)(stream stream))
  (print-unreadable-object (edge stream :type t :identity nil)
    (format stream "VERT:~a->~a FWD:~a BWD:~a" (vertex1 edge) (vertex2 edge)
            (fwd-edge-records edge) (bwd-edge-records edge))))

(defmacro cond-par-edge ((parent-edge child-edge) empty-form fwd-form bwd-form)
  `(cond ((and (null (vertex1 ,parent-edge)) (null (vertex2 ,parent-edge)))
          ,@empty-form)
         ((and (eq (vertex1 ,parent-edge) (vertex1 ,child-edge)) (eq (vertex2 ,parent-edge) (vertex2 ,child-edge)))
          ,@fwd-form)
         ((and (eq (vertex1 ,parent-edge) (vertex2 ,child-edge)) (eq (vertex2 ,parent-edge) (vertex1 ,child-edge)))
          ,@bwd-form)
         (:otherwise (error "parent-edge (~a->~a) and child-edge (~a->~a) vertex sets do not match."
                            (vertex1 ,parent-edge)(vertex2 ,parent-edge)
                            (vertex1 ,child-edge)(vertex2 ,child-edge)))))

(defmethod get-fwd-weight ((edge ebfm-par-edge-list))
  (with-slots ((edge-records fwd-edge-records)) edge
    (if edge-records
        (loop for edge-record in edge-records
           minimize (edge-record-weight edge-record))
        +inf+)))

(defmethod get-bwd-weight ((edge ebfm-par-edge-list))
  (with-slots ((edge-records bwd-edge-records)) edge
    (if edge-records
        (loop for edge-record in edge-records
           minimize (edge-record-weight edge-record))
        +inf+)))


(defmethod member-edge? ((edge ebfm-par-edge-list)(child-edge ebfm-edge) &key (depth 1) (equality #'eql) &allow-other-keys)
  (assert (or (null equality) (functionp equality)))
  (assert (numberp depth))
  (if (> depth 0)
      (or (and equality (funcall equality edge child-edge))
          (some (lambda (record) (member-edge? (edge-record-edge record) child-edge :depth (1- depth) :equality equality))
                (fwd-edge-records edge))
          (some (lambda (record) (member-edge? (edge-record-edge record) child-edge :depth (1- depth) :equality equality))
                (bwd-edge-records edge)))
      nil))

(defmethod add-edge ((edge ebfm-par-edge-list) (child-edge ebfm-dir-edge) &key &allow-other-keys)
  "Adds CHILD-EDGE to EDGE.
   The CHILD-EDGE must not already have a parent."
  (assert (null (parent-edge child-edge)))
  (cond-par-edge (edge child-edge)
                 ((push (make-edge-record :dir t :edge child-edge) (fwd-edge-records edge))
                  (setf (vertex1 edge) (vertex1 child-edge))
                  (setf (vertex2 edge) (vertex2 child-edge)))
                 ((push (make-edge-record :dir t :edge child-edge) (fwd-edge-records edge)))
                 ((push (make-edge-record :dir t :edge child-edge) (bwd-edge-records edge))))
  (set-parent-edge child-edge edge))

(defmethod add-edge ((edge ebfm-par-edge-list) (child-edge ebfm-par-edge-list) &key &allow-other-keys)
  "Adds CHILD-EDGE to EDGE.
   The CHILD-EDGE must not already have a parent."
  (assert (null (parent-edge child-edge)))
  (dolist (edge-record (fwd-edge-records child-edge))
    (set-parent-edge (edge-record-edge edge-record) nil)
    (add-edge edge (edge-record-edge edge-record)))
  (dolist (edge-record (bwd-edge-records child-edge))
    (set-parent-edge (edge-record-edge edge-record) nil)
    (add-edge edge (edge-record-edge edge-record)))
  (set-parent-edge child-edge nil))

(defmethod add-edge ((edge ebfm-par-edge-list) (child-edge ebfm-seq-edge) &key &allow-other-keys)
  "Adds CHILD-EDGE to EDGE.
   The CHILD-EDGE must not already have a parent."
  (assert (null (parent-edge child-edge)))
  (cond-par-edge (edge child-edge)
                 ((push (make-edge-record :dir t :edge child-edge) (fwd-edge-records edge))
                  (push (make-edge-record :dir nil :edge child-edge) (bwd-edge-records edge))
                  (setf (vertex1 edge) (vertex1 child-edge))
                  (setf (vertex2 edge) (vertex2 child-edge)))
                 ((push (make-edge-record :dir t :edge child-edge) (fwd-edge-records edge))
                  (push (make-edge-record :dir nil :edge child-edge) (bwd-edge-records edge)))
                 ((push (make-edge-record :dir nil :edge child-edge) (fwd-edge-records edge))
                  (push (make-edge-record :dir t :edge child-edge) (bwd-edge-records edge))))
  (set-parent-edge child-edge edge))

(defmethod remove-edge ((edge ebfm-par-edge-list) (child-edge ebfm-dir-edge))
  "Removes CHILD-EDGE from EDGE."
  (cond-par-edge (edge child-edge)
                 ((error "Cannot remove child-edge from this edge, because it does not exist."))
                 ((delete child-edge (fwd-edge-records edge) :key #'edge-record-edge))
                 ((delete child-edge (bwd-edge-records edge) :key #'edge-record-edge)))
  (set-parent-edge child-edge nil))

(defmethod remove-edge ((edge ebfm-par-edge-list) (child-edge ebfm-seq-edge))
  "Removes CHILD-EDGE from EDGE."
  (delete child-edge (fwd-edge-records edge) :key #'edge-record-edge)
  (delete child-edge (bwd-edge-records edge) :key #'edge-record-edge)
  (set-parent-edge child-edge nil))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LIST-BASED SEQUENTIAL EDGE CLASS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defclass ebfm-seq-edge-list (ebfm-seq-edge)
  ((edge-records
    :type smart-list
    :initform (make-smart-list)
    :initarg :edge-records
    :accessor edge-records
    :documentation "The set of edges connecting from vertex1 to vertex2.")))

(defmethod print-object ((edge ebfm-seq-edge-list)(stream stream))
  (print-unreadable-object (edge stream :type t :identity nil)
    (format stream "VERT:~a->~a FWD:~a" (vertex1 edge) (vertex2 edge)
            (coll-tolist (edge-records edge) :copy nil))))

(defmacro cond-seq-edge ((parent-edge child-edge) empty-form head2head-form head2tail-form tail2tail-form tail2head-form)
  (let ((v1p (gensym "v1p-"))
        (v2p (gensym "v2p-"))
        (v1c (gensym "v1c-"))
        (v2c (gensym "v2c-")))
    `(with-slots ((,v1p vertex1) (,v2p vertex2)) ,parent-edge
       (with-slots ((,v1c vertex1) (,v2c vertex2)) ,child-edge
         (cond ((and (null ,v1p) (null ,v2p)) ;; no edges
                ,@empty-form)
               ((and (eql ,v1p ,v1c) (not (eql ,v2p ,v2c))) ;; <-p- -c->
                ,@head2head-form)
               ((and (eql ,v1p ,v2c) (not (eql ,v2p ,v1c))) ;; <-p- <-c-
                ,@head2tail-form)
               ((and (eql ,v2p ,v2c) (not (eql ,v1p ,v1c))) ;; -p-> <-c-
                ,@tail2tail-form)
               ((and (eql ,v2p ,v1c) (not (eql ,v1p ,v2c))) ;; -p-> -c->
                ,@tail2head-form)
               (:otherwise
                (error "parent-edge (~a->~a) and child-edge (~a->~a) cannot be combined in sequence."
                      ,v1p ,v2p ,v1c ,v2c)))))))

(defmethod get-fwd-weight ((edge ebfm-seq-edge-list))
  (with-slots ((edge-records edge-records)) edge
    (if (coll-empty? edge-records)
        +inf+
        (loop for edge-record in (coll-tolist edge-records :copy nil)
           sum (edge-record-weight edge-record :reverse nil)))))

(defmethod get-bwd-weight ((edge ebfm-seq-edge-list))
  (with-slots ((edge-records edge-records)) edge
    (if (coll-empty? edge-records)
        +inf+
        (loop for edge-record in (coll-tolist edge-records :copy nil)
           sum (edge-record-weight edge-record :reverse t)))))

(defmethod member-edge? ((edge ebfm-seq-edge-list)(child-edge ebfm-edge) &key (depth 1) (equality #'eql) &allow-other-keys)
  (assert (or (null equality) (functionp equality)))
  (assert (numberp depth))
  (if (> depth 0)
      (or (and equality (funcall equality edge child-edge))
          (some (lambda (record) (member-edge? (edge-record-edge record) child-edge :depth (1- depth) :equality equality))
                (coll-tolist (edge-records edge) :copy nil)))
      nil))

(defmethod member-vertex? ((edge ebfm-seq-edge-list)(vertex symbol))
  (docoll ((edge-records edge) edge-record)
    (let ((child-edge (edge-record-edge edge-record)))
      (when (or (eql (vertex1 child-edge) vertex) (eql (vertex2 child-edge) vertex))
        (return-from member-vertex? t)))))

(defmethod member-vertices ((edge ebfm-seq-edge-list))
  (let ((vertices (make-smart-list))
        (edge-records (edge-records edge)))
    (unless (coll-empty? edge-records)
      ;; add the first vertex
      (with-slots ((child-dir dir)(child-edge edge)) (coll-get-first edge-records)
        (if child-dir
            (coll-add! vertices (vertex1 child-edge))
            (coll-add! vertices (vertex2 child-edge))))
      ;; add the other vertices
      (docoll (edge-records edge-record)
              (with-slots ((child-dir dir)(child-edge edge)) edge-record
                (if child-dir
                    (coll-add! vertices (vertex2 child-edge))
                    (coll-add! vertices (vertex1 child-edge))))) )
    (coll-tolist vertices :copy nil)))

(defmethod add-edge ((edge ebfm-seq-edge-list) (child-edge ebfm-dir-edge)
                     &key (make-par-edge (lambda () (make-instance 'ebfm-par-edge-list)))
                       &allow-other-keys)
  "Adds CHILD-EDGE to EDGE.
   The CHILD-EDGE must not already have a parent."
  (assert (null (parent-edge child-edge)))
  (let ((par-edge (funcall make-par-edge)))
    (add-edge par-edge child-edge)
    (add-edge edge par-edge)))

(defmethod add-edge ((edge ebfm-seq-edge-list) (child-edge ebfm-par-edge) &key &allow-other-keys)
  "Adds CHILD-EDGE to EDGE.
   The CHILD-EDGE must not already have a parent."
  (assert (null (parent-edge child-edge)))
  (cond-seq-edge (edge child-edge)
                 ;; empty form
                 ((coll-add! (edge-records edge) (make-edge-record :dir t :edge child-edge))
                  (setf (vertex1 edge) (vertex1 child-edge))
                  (setf (vertex2 edge) (vertex2 child-edge)))
                 ;; <-p- -c->
                 ((coll-insert! (edge-records edge) 0 (make-edge-record :dir nil :edge child-edge))
                  (setf (vertex1 edge) (vertex2 child-edge)))
                 ;; <-p- <-c-
                 ((coll-insert! (edge-records edge) 0 (make-edge-record :dir t :edge child-edge))
                  (setf (vertex1 edge) (vertex1 child-edge)))
                 ;; -p-> <-c-
                 ((coll-add! (edge-records edge) (make-edge-record :dir nil :edge child-edge))
                  (setf (vertex2 edge) (vertex1 child-edge)))
                 ;; -p-> -c->
                 ((coll-add! (edge-records edge) (make-edge-record :dir t :edge child-edge))
                  (setf (vertex2 edge) (vertex2 child-edge))))
  (set-parent-edge child-edge edge))

(defmethod add-edge ((edge ebfm-seq-edge-list) (child-edge ebfm-seq-edge-list) &key &allow-other-keys)
  "Adds CHILD-EDGE to EDGE.
   The CHILD-EDGE must not already have a parent."
  (assert (null (parent-edge child-edge)))
  ;;(with-slots ((child-edge-records edge-records)) child-edge)
  (let ((child-edge-records (edge-records child-edge)))
    (cond-seq-edge (edge child-edge)
                   ;; empty form
                   ((docoll (child-edge-records child-edge-record)
                            (set-parent-edge (edge-record-edge child-edge-record) nil)
                            (add-edge edge (edge-record-edge child-edge-record))))
                   ;; <-p- -c->
                   ((dolist (child-edge-record (reverse (coll-tolist child-edge-records :copy nil)))
                      (set-parent-edge (edge-record-edge child-edge-record) nil)
                      (add-edge edge (edge-record-edge child-edge-record))))
                   ;; <-p- <-c-
                   ((docoll (child-edge-records child-edge-record)
                            (set-parent-edge (edge-record-edge child-edge-record) nil)
                            (add-edge edge (edge-record-edge child-edge-record))))
                   ;; -p-> <-c-
                   ((dolist (child-edge-record (reverse (coll-tolist child-edge-records :copy nil)))
                      (set-parent-edge (edge-record-edge child-edge-record) nil)
                      (add-edge edge (edge-record-edge child-edge-record))))
                   ;; -p-> -c->
                   ((docoll (child-edge-records child-edge-record)
                            (set-parent-edge (edge-record-edge child-edge-record) nil)
                            (add-edge edge (edge-record-edge child-edge-record))))))
  (set-parent-edge child-edge nil))

(defmethod split-by-vertices ((edge ebfm-seq-edge-list)(vertex-names list)
                               &key (make-seq-edge (lambda () (make-instance 'ebfm-seq-edge-list)))
                                 &allow-other-keys)
  "Given a list of vertices, returns the edge sequence split up into dir-edges, seq-edges, and par-edges.
   The original EDGE cannot have a parent. The children edges are not copied.
   It is not recommended to use EDGE again in the graph."
  (assert (null (parent-edge edge)))
  (let ((divided-edges (make-smart-list)) ;; set of edges to return
        (curr-sequence (make-smart-list))
        (curr (coll-tolist (edge-records edge) :copy nil)))
    (flet ((boundary-vertex? (vertex) (member vertex vertex-names))
           (save-sequence () (cond ((coll-empty? curr-sequence))
                                   ((= (coll-size curr-sequence) 1)
                                    (let ((edge-to-move (coll-get-first curr-sequence)))
                                      (set-parent-edge edge-to-move nil)
                                      (coll-add! divided-edges edge-to-move)))
                                   (:otherwise
                                    (let ((new-seq-edge (funcall make-seq-edge)))
                                      (docoll (curr-sequence edge-to-move)
                                              (set-parent-edge edge-to-move nil)
                                              (add-edge new-seq-edge edge-to-move))
                                      (coll-add! divided-edges new-seq-edge))) )
                          (coll-clear! curr-sequence)))
       ;; This loop method works on the basis that the length of the list VERTEX-NAMES
       ;; is one longer than the number of child-edges in EDGE.
       (loop for member-vertex in (member-vertices edge) do
            (when (boundary-vertex? member-vertex)
              ;; save off the curr-sequence
              (save-sequence))
            (if curr
                (coll-add! curr-sequence (edge-record-edge (car curr)))
                (return))
            (setf curr (cdr curr)))

      ;; save off the remaining sequence
      (loop while curr do
           (coll-add! curr-sequence (edge-record-edge (car curr)))
           (setf curr (cdr curr)))
      (save-sequence)
      ;; return the sequences
      (coll-tolist divided-edges :copy nil))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; EDGE MODIFICATION
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod find-vertex ((g bfm-graph)(name t) &key (default nil))
  "Finds a bfm-vertex object named NAME.
   If the bfm-vertex object exists, it will be returned.
   If not, DEFAULT will be returned (which is nil, by default)."
  (map-get (vertex-map g) name :default default))

(defmethod ensure-vertex ((g bfm-graph)(name t))
  "Ensures a vertex with the given name exists.
   Returns two values. The first value is a bfm-vertex object.
   The second value is t if a vertex was added. nil otherwise."
  (let ((v-obj (map-get (vertex-map g) name :default nil)))
    (if v-obj
        (values v-obj nil)
        (progn
          ;; make the vertex
          (setf v-obj (make-bfm-vertex :name name :d 0))
          ;; add new vertex to the vertex-map
          (map-add! (vertex-map g) name v-obj)
          ;; add an edge to connect the start-vertex to this vertex
          ;; (unless start-vertex
          ;;    (let ((start-edge (make-instance 'bfm-edge :w 0 :to-vertex v-obj)))
          ;;      (coll-add! (out-edges (start-vertex g)) start-edge)
          ;;      (coll-add! (in-vertices v-obj) (start-vertex g))))

          ;; return
          (values v-obj t)))))


(defmethod find-edge ((g bfm-graph)(from-v-name t)(to-v-name t) &key (default nil))
  "Finds a bfm-edge object from a vertex named FROM-V-NAME to a vertex TO-V-NAME.
   If the bfm-edge object exists, it will be returned.
   If not, DEFAULT will be returned (which is nil, by default)."
  (let ((from-v-obj (find-vertex g from-v-name))
        (to-v-obj (find-vertex g to-v-name)))
    (if (and from-v-obj to-v-obj)
        (coll-get (out-edges from-v-obj) to-v-obj :use-key-function nil :default default)
        nil)))

(defmethod ensure-edge ((g bfm-graph)(from-v-name t)(to-v-name t) &key (weight +inf+) (name nil))
  "Ensures an edge exists from a vertex named FROM-V-NAME to a vertex TO-V-NAME.
   If the edge does NOT exist,
    the weight will be initialized to WEIGHT (default: infinity),
    the name will be initialized to NAME.
   Returns two values. The first value is a bfm-edge object.
   The second value is t if an edge was added. nil otherwise."
  ;; coerce weight to be type double-float to fix floating-point error
  (setf weight (coerce weight 'double-float))
  (let* ((from-v-obj (ensure-vertex g from-v-name))
         (to-v-obj (ensure-vertex g to-v-name))
         (edge-obj (coll-get (out-edges from-v-obj) to-v-obj :use-key-function nil)))
    ;; decide what to do if the edge exists already or not.
    (if edge-obj
        ;; if the edge-object exists
        (values edge-obj nil)
        ;; if the edge-object does not exist
        (progn
          (setf edge-obj (make-instance 'bfm-edge :name name :w weight :to-vertex to-v-obj))
          (coll-add! (out-edges from-v-obj) edge-obj)
          (coll-add! (in-vertices to-v-obj) from-v-obj)
          ;; return
          (values edge-obj t)))))






(defmethod add-edge ((g ebfm-graph)(from-v-name t)(to-v-name t)(weight number))
  "Adds a directed-edge that starts at FROM-V-NAME and ends at TO-V-NAME with weight WEIGHT.
   If such an edge does not yet exist, it is created.
   If WEIGHT is positive infinity (+inf+), nothing will happen, and nil will be returned.
   Returns T if an addition occured, nil otherwise.")

(defmethod remove-edge ((g ebfm-graph)(edge ebfm-dir-edge)))

(defmethod remove-edge ((g ebfm-graph)(from-v-name t)(to-v-name t) &optional (weight number))
  "Removes a directed-edge that starts at FROM-V-NAME, ends at TO-V-NAME, with optional weight WEIGHT.
   If the optional WEIGHT is not provided, all matching directed-edges will be removed.
   Returns T if the removal occured, nil otherwise.")


(defmethod update-edge ((g ebfm-graph)(edge ebfm-dir-edge)(new-weight number)))

(defmethod update-edge ((g ebfm-graph)(from-v-name t)(to-v-name t)(old-weight number)(new-weight number))
  "Updates the weight of a directed-edge that starts at FROM-V-NAME, ends at TO-V-NAME, and
   has the weight OLD-WEIGHT, to NEW-WEIGHT.
   Returns T if the update occured, nil otherwise.")

(defmethod modify-edge ((g ebfm-graph)(from-v-name t)(to-v-name t)(new-weight number))
  "Changes the weight of all directed edges that start at FROM-V-NAME and end at TO-V-NAME
   to NEW-WEIGHT. Where FROM-V-NAME and TO-V-NAME are vertex names, not vertex objects.
   If NEW-WEIGHT is positive infinity (+inf+), the edge is removed from the graph.
   Otherwise, the edge is created if it does not exist.
   Returns T if the update occured, nil otherwise."
  (error "unsupported function"))
