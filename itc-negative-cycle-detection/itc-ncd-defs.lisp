;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :itc-v2)

(defclass itc-negative-cycle-detection (itc-checker)
  ((graph
    :type (or null bfm-graph)
    :initform nil
    :initarg :graph
    :accessor itc-ncd-graph
    :documentation "The graph on which Bellman-Ford-Moore operates.")
   (constraint-map
    :type hashmap
    :initform (make-hashmap :test #'equal)
    :initarg :constraint-map
    :accessor itc-ncd-constraint-map
    :documentation "Associates a list of events of the form '(e1 e2) to a heap ~
                  of elements '(constraint :lb) or '(constraint :ub)."))
  (:documentation "ITC checker that uses a Bellman-Ford-Moore negative cycle detection strategy."))

(defmethod initialize-instance :after ((itc itc-negative-cycle-detection)
                                       ;; &rest initargs ; BCW - Not used.  Removed to eliminate warnings.
                                       &key (type-of-a-queue :fifo) (type-of-b-queue :fifo))
  ;; Raise an error if network contains anything other than simple-temporal-constraints
  (unless (equal (list :simple-temporal-constraints) (features (itc-temporal-network itc)))
    (error "ITC is unable to handle this network. Most likely because it contains contingent constraints."))
  (itc-initialize! itc :type-of-a-queue type-of-a-queue :type-of-b-queue type-of-b-queue))
