;;; Copyright (c) 2013, MIT.

;;; This software may not be redistributed, and can only be retained
;;; and used with the explicit written consent of the author,
;;; Brian C. Williams.

;;; This software is made available as is; no guarantee is provided
;;; with respect to performance or correct behavior.

;;; This software may only be used for non-commercial, non-profit,
;;; research activities.

(in-package :asdf-user)

(asdf:defsystem #:itc-v2
  :name "Incremental Temporal Consistency"
  :version (:read-file-form "version.lisp-expr")
  :description "Contains algorithms for incrementally testing temporal
                consistency of temporal networks using negative cycle detection."
  :maintainer "MIT MERS Group"
  :components ((:file "package")
               (:file "debug-print" :depends-on ("package"))
               (:file "itc-defs" :depends-on ("package"))
               ;; (:module "itc-linear-program"
               ;;  :components ((:file "itc-linear-program-defs")
               ;;               (:file "initialize" :depends-on ("itc-linear-program-defs"))
               ;;               (:file "consistency" :depends-on ("itc-linear-program-defs"))
               ;;               (:file "temporal-network-hooks" :depends-on ("itc-linear-program-defs")))
               ;;  :depends-on ("itc-defs"))
               (:module "itc-negative-cycle-detection"
                :components (
                             (:file "bfm-graph")
                             ;; (:file "efficient-bfm-graph" :depends-on ("bfm-graph"))
                             (:file "bellman-ford-moore"
                              :depends-on ("bfm-graph" "itc-ncd-queue"))
                             (:file "itc-ncd-queue" :depends-on ("bfm-graph"))
                             (:file "itc-ncd-defs" :depends-on ("bfm-graph"))
                             (:file "temporal-network-hooks"
                              :depends-on ("itc-ncd-defs"
                                           "bellman-ford-moore"
                                           "bfm-graph")))
                :depends-on ("itc-defs"))
               (:file "itc" :depends-on ("itc-negative-cycle-detection"))
               (:file "itc-example" :depends-on ("itc")))
  :in-order-to ((test-op (load-op #:itc-v2/tests)))
  :perform (test-op :after (op c)
                    (funcall (read-from-string "fiveam:run!") :itc))
  :depends-on (#:temporal-networks
               ;; Commented out because it doesn't work on all platforms yet.
               ;; #:numeric-optimization
               #:abstract-data-types
               ;; Third party
               #:float-features
               #:log4cl #:cl-heap
               #:trivial-garbage
               #:fiveam))

(asdf:defsystem #:itc-v2/tests
  :name "Incremental Temporal Consistency Tests"
  :version (:read-file-form "version.lisp-expr")
  :pathname "tests"
  :serial t
  :components ((:file "package")
               (:file "test-suite")

               ;; Internal workings tests.
               (:file "internal-bfm-graph")
               ;; (:file "internal-efficient-bfm-graph")

               ;; Public interface tests.
               ;; (:file "public-lp-tests")
               (:file "public-ncd-tests")
               (:file "public-burning-tests")

               ;; Regression tests.
               (:file "stn2dot")
               (:file "regression-kirk-zipcar-9"))

  :depends-on (#:itc-v2
               #:fiveam
               #:simple-dot))

#+:mtk-documentation
(eval-when (:compile-toplevel :load-toplevel :execute)
  (mtk-doc:register-doc-generator (:itc :itc-v2)
    (:apiref :itc-v2)))
