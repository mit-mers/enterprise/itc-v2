(in-package :itc-v2/tests/internal)

(in-suite :itc-ncd-eff-bfm)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; HELPER TEST FUNCTIONS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defmethod verify-par-edge ((edge ebfm-par-edge-list)(fwd-child-edges list)(bwd-child-edges list))
  "Checks whether the parallel EDGE contains the edges in forward and backward child-edges.
   Returns two values.
   If EDGE is valid, returns t and an empty string.
   If EDGE is invalid, returns nil and an explanation string."
  (assert (= (length fwd-child-edges) (length (remove-duplicates fwd-child-edges))))
  (assert (= (length bwd-child-edges) (length (remove-duplicates bwd-child-edges))))
  ;; copy the input, so we can destructively modify it
  (setf fwd-child-edges (copy-list fwd-child-edges))
  (setf bwd-child-edges (copy-list bwd-child-edges))
  ;; CHECK FORWARD EDGES
  (with-slots ((v1p vertex1) (v2p vertex2)) edge
    (dolist (record (fwd-edge-records edge))
      (with-slots ((child-dir dir) (child-edge edge)) record
        ;; verify direction
        (unless  (or (and child-dir (eql v1p (vertex1 child-edge)) (eql v2p (vertex2 child-edge)))
                     (and (not child-dir)(eql v1p (vertex2 child-edge))(eql v2p (vertex1 child-edge))))
          (return-from verify-par-edge (values nil (format nil "The forward child-edge ~a is pointing in the wrong direction." child-edge))))
        ;; verify the child-edge exists
        (unless (member child-edge fwd-child-edges)
          (return-from verify-par-edge (values nil (format nil "The edge ~a contains unexpected forward child-edge ~a." edge child-edge))))
        ;; delete the child-edge
        (setf fwd-child-edges (delete child-edge fwd-child-edges))))
    ;; verify the edges only contains exactly child edges
    (unless fwd-child-edges
      (values nil "The edge ~a does not contain all the required forward child-edges. It is missing: ~a" edge fwd-child-edges))
    ;; CHECK BACKWARD EDGES
    (dolist (record (bwd-edge-records edge))
      (with-slots ((child-dir dir) (child-edge edge)) record
        ;; verify direction
        (unless  (or (and child-dir (eql v1p (vertex2 child-edge)) (eql v2p (vertex1 child-edge)))
                     (and (not child-dir)(eql v1p (vertex1 child-edge))(eql v2p (vertex2 child-edge))))
          (return-from verify-par-edge (values nil (format nil "The backward child-edge ~a is pointing the wrong direction." child-edge))))
        ;; verify the child-edge exists
        (unless (member child-edge bwd-child-edges)
          (return-from verify-par-edge (values nil (format nil "The edge ~a contains unexpected backward child-edge ~a." edge child-edge))))
        ;; delete the child-edge
        (setf bwd-child-edges (delete child-edge bwd-child-edges))))
    ;; verify the edges only contains exactly child edges
    (unless bwd-child-edges
      (values nil "The edge ~a does not contain all the required backward child-edges. It is missing: ~a" edge bwd-child-edges)))
  (values t ""))

(defun is-correct-par-edge (par-edge fwd-child-edges bwd-child-edges fwd-weight bwd-weight)
  "Implements three fiveam tests.
   * Tests whether the parallel edge PAR-EDGE contains all the expected forward and backward child edges.
   * Tests if the forward weight is correct.
   * Tests if the backward weight is correct."
  ;; verify the edges of correct
  (multiple-value-bind (verified errmsg)
      (verify-par-edge par-edge fwd-child-edges bwd-child-edges)
    (is-true verified errmsg))
  ;; verify the forward and backward weights
  (is (= (get-fwd-weight par-edge) fwd-weight))
  (is (= (get-bwd-weight par-edge) bwd-weight)))

(defmethod verify-seq-edge ((edge ebfm-seq-edge-list)(child-edges list))
  "Checks whether the sequential EDGE contains the CHILD-EDGES.
   Returns two values.
   If EDGE is valid, returns t and an empty string.
   If EDGE is invalid, returns nil and an explanation string."
  ;; copy the input, so we can destructively modify it
  (block nil
    (setf child-edges (copy-list child-edges))
    ;; verify the edges only contains exactly child edges
    (unless (eql (length child-edges) (coll-size (edge-records edge)))
      (values nil "TEST ERROR: The number of child-edges in sequential edge ~a needs to be the same length as the CHILD-EDGES list." edge))
    ;; CHECK SEQUENTIAL EDGES
    (let ((curr-vertex (vertex1 edge)))
      (loop for record in (coll-tolist (edge-records edge) :copy t)
         for exp-child-edge in child-edges do
           (with-slots ((child-dir dir) (child-edge edge)) record
             ;; verify direction
             (if child-dir
                 (if (eql (vertex1 child-edge) curr-vertex)
                     (setf curr-vertex (vertex2 child-edge)) ;; traverse the next vertex
                     (return (values nil (format nil "The child-edge ~a is pointing in the wrong direction." child-edge))))
                 (if (eql (vertex2 child-edge) curr-vertex)
                     (setf curr-vertex (vertex1 child-edge)) ;; traverse the next vertex
                     (return (values nil (format nil "The child-edge ~a is pointing in the wrong direction." child-edge)))))
             ;; verify the child-edge exists (and it must be a parallel edge)
             (typecase exp-child-edge
               (ebfm-dir-edge
                (if (and (typep child-edge 'ebfm-par-edge) (member-edge? child-edge exp-child-edge :depth 1))
                    (setf child-edges (delete child-edge child-edges))
                    (return (values nil "The edge ~a does not contain child-edge ~a." edge exp-child-edge))))
               (ebfm-par-edge
                (unless (eql child-edge exp-child-edge)
                  (return (values nil "The edge ~a does not contain child-edge ~a." edge exp-child-edge))))
               (ebfm-seq-edge
                (return (values nil "TEST ERROR: Cannot check if a sequential edge is a member of a sequential edge.")))))))
    ;; verify the edges only contains exactly child edges
    (unless child-edges
      (values nil "The edge ~a does not contain all the required child-edges. It is missing: ~a" edge child-edges))
    (values t "")))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; TESTS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun is-correct-seq-edge (seq-edge child-edges fwd-weight bwd-weight)
  "Implements three fiveam tests.
   * Tests whether the parallel edge SEQ-EDGE contains all the expected forward and backward child edges.
   * Tests if the forward weight is correct.
   * Tests if the backward weight is correct."
  ;; verify the edges of correct
  (multiple-value-bind (verified errmsg)
      (verify-seq-edge seq-edge child-edges)
    (is-true verified errmsg))
  ;; verify the forward and backward weights
  (is (= (get-fwd-weight seq-edge) fwd-weight))
  (is (= (get-bwd-weight seq-edge) bwd-weight)))

(def-test test-par-edge ()
  (let ((par-edge-top (make-instance 'ebfm-par-edge-list))
        (dir-edge1 (make-instance 'ebfm-dir-edge :vertex1 'a :vertex2 'b :weight 6))
        (dir-edge2 (make-instance 'ebfm-dir-edge :vertex1 'b :vertex2 'a :weight 5))
        (dir-edge3 (make-instance 'ebfm-dir-edge :vertex1 'b :vertex2 'a :weight 4))
        (dir-edge4 (make-instance 'ebfm-dir-edge :vertex1 'a :vertex2 'b :weight 3))
        (dir-edge5 (make-instance 'ebfm-dir-edge :vertex1 'b :vertex2 'c :weight 2))
        (dir-edge6 (make-instance 'ebfm-dir-edge :vertex1 'c :vertex2 'a :weight 1))
        (par-edge1 (make-instance 'ebfm-par-edge-list))
        (seq-edge1 (make-instance 'ebfm-seq-edge-list)))
    ;; par-edge-top: a->b
    (add-edge par-edge-top dir-edge1)
    (is-correct-par-edge par-edge-top `(,dir-edge1) `() 6 +inf+)

    (add-edge par-edge-top dir-edge2)
    (is-correct-par-edge par-edge-top `(,dir-edge1) `(,dir-edge2) 6 5)

    ;; should cause errors: dir-edge2 already has parent
    (signals error (add-edge par-edge-top dir-edge2))
    ;; should cause errors: dir-edge5 is not in the same vertex set
    (signals error (add-edge par-edge-top dir-edge5))

    ;; par-edge1: b->a
    (add-edge par-edge1 dir-edge3)
    (add-edge par-edge1 dir-edge4)
    (is-correct-par-edge par-edge1 `(,dir-edge3) `(,dir-edge4) 4 3)
    (add-edge par-edge-top par-edge1)
    (is-correct-par-edge par-edge-top `(,dir-edge1 ,dir-edge4) `(,dir-edge2 ,dir-edge3) 3 4)

    ;; seq-edge1: b->c->a
    (add-edge seq-edge1 dir-edge5)
    (add-edge seq-edge1 dir-edge6)
    (is-correct-seq-edge seq-edge1 `(,dir-edge5 ,dir-edge6) 3 +inf+)
    (add-edge par-edge-top seq-edge1)
    (is-correct-par-edge par-edge-top `(,dir-edge1 ,dir-edge4 ,seq-edge1) `(,dir-edge2 ,dir-edge3 ,seq-edge1) 3 3)))

(def-test test-seq-edge-add ()
  (let ((seq-edge-top (make-instance 'ebfm-seq-edge-list))
        (dir-edge1 (make-instance 'ebfm-dir-edge :vertex1 'd :vertex2 'e :weight 6))
        (dir-edge2 (make-instance 'ebfm-dir-edge :vertex1 'e :vertex2 'f :weight 5))
        (dir-edge3 (make-instance 'ebfm-dir-edge :vertex1 'd :vertex2 'c :weight 4))
        (dir-edge4 (make-instance 'ebfm-dir-edge :vertex1 'c :vertex2 'b :weight 3))
        (dir-edge5 (make-instance 'ebfm-dir-edge :vertex1 'b :vertex2 'a :weight 2))
        (dir-edge6 (make-instance 'ebfm-dir-edge :vertex1 'b :vertex2 'a :weight 1))
        (dir-edge7 (make-instance 'ebfm-dir-edge :vertex1 'a :vertex2 'b :weight 1))
        (dir-edge8 (make-instance 'ebfm-dir-edge :vertex1 'g :vertex2 'f :weight 1))
        (dir-edge9 (make-instance 'ebfm-dir-edge :vertex1 'h :vertex2 'g :weight 1))
        (dir-edge10 (make-instance 'ebfm-dir-edge :vertex1 'h :vertex2 'i :weight 1))

        (par-edge1 (make-instance 'ebfm-par-edge-list))
        (par-edge2 (make-instance 'ebfm-par-edge-list))
        (seq-edge1 (make-instance 'ebfm-seq-edge-list)))

    ;; seq-edge-top: b<-c<-d->e->f
    (add-edge seq-edge-top dir-edge1)
    (add-edge seq-edge-top dir-edge2)
    (add-edge seq-edge-top dir-edge3)
    (add-edge seq-edge-top dir-edge4)
    (is-correct-seq-edge seq-edge-top `(,dir-edge4 ,dir-edge3 ,dir-edge1 ,dir-edge2) +inf+ +inf+)

    ;; seq-edge-top: a<-b<-c<-d->e->f
    (add-edge par-edge1 dir-edge5)
    (add-edge par-edge1 dir-edge6)
    (add-edge par-edge1 dir-edge7)
    (add-edge seq-edge-top par-edge1)
    (is-correct-seq-edge seq-edge-top `(,par-edge1 ,dir-edge4 ,dir-edge3 ,dir-edge1 ,dir-edge2) +inf+ +inf+)

    ;; seq-edge-top: a<-b<-c<-d->e->f<-g
    (add-edge par-edge2 dir-edge8)
    (add-edge seq-edge-top par-edge2)
    (is-correct-seq-edge seq-edge-top `(,par-edge1 ,dir-edge4 ,dir-edge3 ,dir-edge1 ,dir-edge2 ,par-edge2) +inf+ +inf+)

    ;; seq-edge-top: a<-b<-c<-d->e->f<-g<-h->i
    (add-edge seq-edge1 dir-edge9)
    (add-edge seq-edge1 dir-edge10)
    (add-edge seq-edge-top seq-edge1)
    (is-correct-seq-edge seq-edge-top `(,par-edge1 ,dir-edge4 ,dir-edge3 ,dir-edge1 ,dir-edge2 ,par-edge2, dir-edge9, dir-edge10) +inf+ +inf+)))

(def-test test-seq-edge-weight ()
  (let ((seq-edge-top (make-instance 'ebfm-seq-edge-list))
        (dir-edge2 (make-instance 'ebfm-dir-edge :vertex1 'f :vertex2 'e :weight 5))
        (dir-edge1 (make-instance 'ebfm-dir-edge :vertex1 'e :vertex2 'd :weight 6))
        (dir-edge3 (make-instance 'ebfm-dir-edge :vertex1 'd :vertex2 'c :weight 4))
        (dir-edge4 (make-instance 'ebfm-dir-edge :vertex1 'c :vertex2 'b :weight 3))

        (dir-edge5 (make-instance 'ebfm-dir-edge :vertex1 'a :vertex2 'b :weight 2))
        (dir-edge6 (make-instance 'ebfm-dir-edge :vertex1 'b :vertex2 'a :weight 1))
        (dir-edge7 (make-instance 'ebfm-dir-edge :vertex1 'a :vertex2 'b :weight 1))

        (par-edge1 (make-instance 'ebfm-par-edge-list)))

    ;; seq-edge-top: f->e->d->c->b
    (add-edge seq-edge-top dir-edge1)
    (add-edge seq-edge-top dir-edge2)
    (add-edge seq-edge-top dir-edge3)
    (add-edge seq-edge-top dir-edge4)
    (is-correct-seq-edge seq-edge-top `(,dir-edge1 ,dir-edge2 ,dir-edge3 ,dir-edge4) 18 +inf+)

    ;; seq-edge-top: f->e->d->c->b<-a
    ;; par-edge: a->b
    (add-edge par-edge1 dir-edge5)
    (add-edge par-edge1 dir-edge6)
    (add-edge par-edge1 dir-edge7)
    (add-edge seq-edge-top par-edge1)
    (is-correct-seq-edge seq-edge-top `(,dir-edge1 ,dir-edge2 ,dir-edge3 ,dir-edge4 ,par-edge1) 19 +inf+)))

(def-fixture test-seq-edge-split-fixture ()
  (let ((seq-edge-top (make-instance 'ebfm-seq-edge-list))
        (dir-edge1 (make-instance 'ebfm-dir-edge :vertex1 'a :vertex2 'b :weight 1))
        (dir-edge2 (make-instance 'ebfm-dir-edge :vertex1 'b :vertex2 'c :weight 2))
        (dir-edge3 (make-instance 'ebfm-dir-edge :vertex1 'c :vertex2 'd :weight 3))
        (dir-edge4 (make-instance 'ebfm-dir-edge :vertex1 'e :vertex2 'd :weight 4)) ;; part of parallel edge
        (dir-edge5 (make-instance 'ebfm-dir-edge :vertex1 'd :vertex2 'e :weight 3)) ;; part of parallel edge
        (dir-edge6 (make-instance 'ebfm-dir-edge :vertex1 'e :vertex2 'd :weight 2)) ;; part of parallel edge
        (dir-edge7 (make-instance 'ebfm-dir-edge :vertex1 'e :vertex2 'f :weight 4))
        (par-edge1 (make-instance 'ebfm-par-edge-list)))
    ;; seq-edge-top: a->b->c->d
    (add-edge seq-edge-top dir-edge1)
    (add-edge seq-edge-top dir-edge2)
    (add-edge seq-edge-top dir-edge3)
    ;; seq-edge-top: a->b->c->d<-e
    ;; par-edge: e->d
    (add-edge par-edge1 dir-edge4)
    (add-edge par-edge1 dir-edge5)
    (add-edge par-edge1 dir-edge6)
    (add-edge seq-edge-top par-edge1)
    ;; seq-edge-top: a->b->c->d<-e->f
    (add-edge seq-edge-top dir-edge7)
    (is-correct-seq-edge seq-edge-top `(,dir-edge1 ,dir-edge2 ,dir-edge3 ,par-edge1, dir-edge7) 13 +inf+)
    (&body)))

(def-test test-seq-edge-split ()
  (with-fixture test-seq-edge-split-fixture ()
    (let ((split-edges (split-by-vertices seq-edge-top '(a f))))
      ;; (format t "~a~%" split-edges)
      ;; check the number of split-edges
      (is (= (length split-edges) 1))
      ;; check the membership of the split-edges
      (is (member-edge? (first split-edges) dir-edge1 :depth +inf+))
      (is (member-edge? (first split-edges) dir-edge2 :depth +inf+))
      (is (member-edge? (first split-edges) dir-edge3 :depth +inf+))
      (is (member-edge? (first split-edges) par-edge1 :depth +inf+))
      (is (member-edge? (first split-edges) dir-edge7 :depth +inf+))))
  (with-fixture test-seq-edge-split-fixture ()
    (let ((split-edges (split-by-vertices seq-edge-top '(c d))))
      ;; (format t "~a~%" split-edges)
      ;; check the number of split-edges
      (is (= (length split-edges) 3))
      ;; check the membership of the split-edges
      (is (member-edge? (first split-edges) dir-edge1 :depth +inf+))
      (is (member-edge? (first split-edges) dir-edge2 :depth +inf+))
      (is (member-edge? (second split-edges) dir-edge3 :depth +inf+))
      (is (member-edge? (third split-edges) par-edge1 :depth +inf+))
      (is (member-edge? (third split-edges) dir-edge7 :depth +inf+))))
  (with-fixture test-seq-edge-split-fixture ()
    (let ((split-edges (split-by-vertices seq-edge-top '(c d e))))
      ;; (format t "~a~%" split-edges)
      ;; check the number of split-edges
      (is (= (length split-edges) 4))
      ;; check the membership of the split-edges
      (is (member-edge? (first split-edges) dir-edge1 :depth +inf+))
      (is (member-edge? (first split-edges) dir-edge2 :depth +inf+))
      (is (member-edge? (second split-edges) dir-edge3 :depth +inf+))
      (is (member-edge? (third split-edges) par-edge1 :depth +inf+))
      (is (member-edge? (fourth split-edges) dir-edge7 :depth +inf+))))
  (with-fixture test-seq-edge-split-fixture ()
    (let ((split-edges (split-by-vertices seq-edge-top '(c d e f))))
      ;; (format t "~a~%" split-edges)
      ;; check the number of split-edges
      (is (= (length split-edges) 4))
      ;; check the membership of the split-edges
      (is (member-edge? (first split-edges) dir-edge1 :depth +inf+))
      (is (member-edge? (first split-edges) dir-edge2 :depth +inf+))
      (is (member-edge? (second split-edges) dir-edge3 :depth +inf+))
      (is (member-edge? (third split-edges) par-edge1 :depth +inf+))
      (is (member-edge? (fourth split-edges) dir-edge7 :depth +inf+)))))
