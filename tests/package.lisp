(in-package #:cl-user)

(defpackage #:itc-v2/tests
  (:use #:cl
        #:temporal-networks
        #:itc-v2
        #:fiveam))

(defpackage #:itc-v2/tests/internal
  (:use #:cl
        #:temporal-networks
        #:itc-v2
        #:fiveam)
  (:import-from #:itc-v2
                #:make-queue
                #:enqueue!
                #:dequeue!
                #:isempty?
                #:empty!
                #:make-bfm-vertex
                #:next-set-vertex
                #:in-set
                #:child
                #:set-parent
                #:debug-is-child
                #:delete-subtree
                #:change-start-vertex))

(defpackage #:itc-v2/tests/regression
  (:use #:cl
        #:temporal-networks
        #:itc-v2
        #:fiveam)
  (:import-from #:abstract-data-types
                #:make-hashmap
                #:map-contains?
                #:map-add!)
  (:import-from #:itc-v2
                #:itc-ncd-graph
                #:set-a
                #:set-b
                #:tolist))
