;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :itc-v2/tests)

(def-suite :itc
           :description "Umbrella test suite for the ITC package.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Public tests stay in the ITC-V2/TESTS/INTERNAL package.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def-suite :itc-lp
           :in :itc
           :description "Public interface tests for the LP backend.")
(def-suite :itc-ncd
           :in :itc
           :description "Public interface tests for the negative cycle
                         detection (NCD) backend.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Tests in these suites should be defined in the
;;; ITC-V2/TESTS/INTERNAL package.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def-suite :itc-ncd-internals
           :in :itc
           :description "Tests for the internal workings of the NCD backend.")
(def-suite :itc-ncd-bfm
           :in :itc-ncd-internals
           :description "Test suite for the low-level Bellman-Ford-Moore data
                         structures used in the NCD backend.")
(def-suite :itc-ncd-eff-bfm
           :in :itc-ncd-internals
           :description "Test suite for the low-level Efficient Bellman-Ford-
                         Moore data structures.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Tests in these suites should be defined in the
;;; ITC-V2/TESTS/REGRESSION package.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(def-suite :itc-regressions
           :in :itc
           :description "Tests for documenting and fixing regressions
                         previously introduced.")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Common test fixtures.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(in-suite :itc)

(def-fixture empty-stn ()
  (let ((*stn* (make-simple-temporal-network (gensym "TEST-FIXTURE-TN"))))
    (&body)))

(def-fixture itc-and-stn ()
  (let* ((*stn* (make-simple-temporal-network (gensym "TEST-FIXTURE-STN"))))
    (setf (default-consistency-checker *stn*) 'itc-negative-cycle-detection)
    (&body)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Constants that are also found in itc-v2
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defconstant +inf+ float-features:double-float-positive-infinity)
(defconstant +-inf+ float-features:double-float-negative-infinity)
