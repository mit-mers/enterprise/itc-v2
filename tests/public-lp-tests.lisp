;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :itc-v2/tests)

(in-suite :itc-lp)

(def-test two-event-test ()
  ;; Every two event stn is consistent.
  (for-all ((lb (gen-integer :max 500 :min -500))
            (ub (gen-integer :max 500 :min -500)))
    (with-fixture itc-and-stn (:linear-program)
      (add-temporal-constraint! *stn* (make-temporal-constraint
                                       :e1 :e2 (make-bounded-temporal-duration lb ub)))
      (is (temporal-network-consistent? *itc*)))))

(def-test three-event-test ()
  (for-all ((lb (gen-integer :min 0 :max 50))
            (ub (gen-integer :min 0 :max 50)))
    ;; Everything in this range should be feasible.
    (with-fixture itc-and-stn (:linear-program)
      (add-temporal-constraint! *stn* (make-temporal-constraint
                                       :e1 :e3 (make-bounded-temporal-duration 0 50)))
      (add-temporal-constraint! *stn* (make-temporal-constraint
                                       :e1 :e2 (make-bounded-temporal-duration 0 0)))
      (add-temporal-constraint! *stn* (make-temporal-constraint
                                       :e2 :e3 (make-bounded-temporal-duration lb ub)))
      (is (temporal-network-consistent? *itc*))))
  (for-all ((lb (gen-integer :max -1))
            (ub (gen-integer :max -1)))
    ;; Everything in this range should not be feasible.
    (with-fixture itc-and-stn (:linear-program)
      (add-temporal-constraint! *stn* (make-temporal-constraint
                                       :e1 :e3 (make-bounded-temporal-duration 0 50)))
      (add-temporal-constraint! *stn* (make-temporal-constraint
                                       :e1 :e2 (make-bounded-temporal-duration 0 0)))
      (add-temporal-constraint! *stn* (make-temporal-constraint
                                       :e2 :e3 (make-bounded-temporal-duration lb ub)))
      (is (not (temporal-network-consistent? *itc*)))))
  (for-all ((lb (gen-integer :min 51))
            (ub (gen-integer :min 51)))
    ;; Everything in this range should not be feasible.
    (with-fixture itc-and-stn (:linear-program)
      (add-temporal-constraint! *stn* (make-temporal-constraint
                                       :e1 :e3 (make-bounded-temporal-duration 0 50)))
      (add-temporal-constraint! *stn* (make-temporal-constraint
                                       :e1 :e2 (make-bounded-temporal-duration 0 0)))
      (add-temporal-constraint! *stn* (make-temporal-constraint
                                       :e2 :e3 (make-bounded-temporal-duration lb ub)))
      (is (not (temporal-network-consistent? *itc*))))))

(def-test update-bounds-test ()
  (with-fixture itc-and-stn (:linear-program)
    (add-temporal-constraint! *stn* (make-temporal-constraint
                                     :e1 :e3 (make-bounded-temporal-duration 0 50)))
    (add-temporal-constraint! *stn* (make-temporal-constraint
                                     :e1 :e2 (make-bounded-temporal-duration 0 0)))
    (add-temporal-constraint! *stn* (make-temporal-constraint
                                     :e2 :e3 (make-bounded-temporal-duration -10 -1)
                                     :test-constraint))
    ;; At first, this should NOT be consistent.
    (is (not (temporal-network-consistent? *itc*)))
    ;; Then change the upper bound of the test-constraint to make it consistent.
    (setf (duration-upper-bound (temporal-constraint-duration
                                 (find-temporal-constraint *stn* :test-constraint)))
          25)
    (is (temporal-network-consistent? *itc*))
    ;; Now shift the entire constraint up so that it's inconsistent.
    (setf (duration-lower-bound (temporal-constraint-duration
                                 (find-temporal-constraint *stn* :test-constraint)))
          100)
    (setf (duration-upper-bound (temporal-constraint-duration
                                 (find-temporal-constraint *stn* :test-constraint)))
          150)
    (is (not (temporal-network-consistent? *itc*)))))

(def-test lp-multiple-constraints-same-event-pair ()
  (with-fixture itc-and-stn (:linear-program)
    (add-temporal-constraint! *stn* (make-temporal-constraint
                                     :e1 :e2 (make-bounded-temporal-duration 1 100)))
    (add-temporal-constraint! *stn* (make-temporal-constraint
                                     :e1 :e2 (make-bounded-temporal-duration -100 -10)))
    ;; This should be inconsistent.
    (is (not (temporal-network-consistent? *itc*)))
    ;; Add a third, just to be sure.
    (add-temporal-constraint! *stn* (make-temporal-constraint
                                     :e1 :e2 (make-bounded-temporal-duration -150 200)))
    (is (not (temporal-network-consistent? *itc*)))))

(def-test lp-self-loop ()
  (with-fixture itc-and-stn (:linear-program)
    (add-temporal-constraint! *stn* (make-temporal-constraint
                                     :e1 :e1 (make-bounded-temporal-duration 0 0)))
    ;; This should be consistent.
    (is (temporal-network-consistent? *itc*)))
  (with-fixture itc-and-stn (:linear-program)
    (add-temporal-constraint! *stn* (make-temporal-constraint
                                     :e1 :e1 (make-bounded-temporal-duration 10 100)))
    ;; This should certainly not be consistent.
    (is (not (temporal-network-consistent? *itc*)))))

(def-test lp-fully-connected-graph ()
  (with-fixture itc-and-stn (:linear-program)
    (let ((events (list :e1 :e2 :e3 :e4 :e5))
          (lb 5)
          (ub 8)
          (counter 0))
      (dolist (event1 events)
        (dolist (event2 events)
          (unless (eq event1 event2)
            (incf counter)
            (add-temporal-constraint! *stn*
                                      (make-temporal-constraint
                                       event1 event2
                                       (make-bounded-temporal-duration lb ub)))
            (if (<= counter 4)
                (is (temporal-network-consistent? *itc*))
                (is (not (temporal-network-consistent? *itc*))))))))))
