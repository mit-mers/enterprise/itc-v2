(in-package :itc-v2/tests/regression)

(defmethod dot-print-stn ((stn temporal-network) (outfile string)
                          &key (negcycle nil) (set-a-before nil) (set-b-before nil))
  (simple-dot:with-output-to-dot (stream outfile)
    (stn-to-dot-stream stn :stream stream :negcycle negcycle
                           :set-a-before set-a-before :set-b-before set-b-before)))

(defmethod dot-print-stn ((stn temporal-network) (outfile pathname)
                          &key (negcycle nil) (set-a-before nil) (set-b-before nil))
  (simple-dot:with-output-to-dot (stream outfile)
    (stn-to-dot-stream stn :stream stream :negcycle negcycle
                           :set-a-before set-a-before :set-b-before set-b-before)))

(defmethod stn-to-dot-stream ((stn temporal-network)
                              &key (stream *standard-output*) (indent 2)
                                   (rankdir "LR") (negcycle nil)
                                   (set-a-before nil) (set-b-before nil))
  (simple-dot:with-indent-scope (iprint iprintln :default-indent indent :stream stream)
    (iprintln "digraph G {")
    (simple-dot:with-indent ()
      (iprintln "rankdir=~a" rankdir)
      (do-events (event stn)
        (let ((member-of-a (find (id event) (mapcar #'id set-a-before)))
              (member-of-b (find (id event) (mapcar #'id set-b-before))))
          (iprintln "\"~a\" [label = \"~a\", shape = ~a, fixedsize = true, ~
                             width = 0.75, height = 0.75]"
                    (id event)
                    (format nil "~a" (name event))
                    (cond (member-of-a "doubleoctagon")
                          (member-of-b "doublecircle")
                          (:otherwise "circle")))))
      (do-constraints (constraint stn)
        (let* ((negcycle-ub-involved (find (list constraint :ub) negcycle :test #'equal))
               (negcycle-lb-involved (find (list constraint :lb) negcycle :test #'equal)))
          (iprintln "\"~a\" -> \"~a\" [label =  \"~a\" color = ~s]"
                    (id (from-event constraint))
                    (id (to-event constraint))
                    (get-pretty-number (upper-bound constraint))
                    (if negcycle-ub-involved "black:black" "black"))
          (iprintln "\"~a\" -> \"~a\" [label =  \"~a\" color = ~s]"
                    (id (to-event constraint))
                    (id (from-event constraint))
                    (get-pretty-number (* -1 (lower-bound constraint)))
                    (if negcycle-lb-involved "black:black" "black")))))
    (iprintln "}")))

(defun get-pretty-number (val)
  "Helper function to pretty-print infinities"
  (cond ((= val +inf+) "inf") ;; temporal-networks/serialization/xml:*positive-infinity*
        ((= val +-inf+) "-inf") ;; temporal-networks/serialization/xml:*negative-infinity*
        (t (format nil "~a" val))))

;; (defmethod dot-print-stn-pair ((tn temporal-network)
;;                                (tn2 temporal-network)
;;                                (outfile string))
;;   (simple-dot:with-output-to-dot (stream outfile)
;;     (stn-pair-to-dot-stream tn tn2 stream)))

;; (defmethod dot-print-stn-pair ((tn temporal-network)
;;                                (tn2 temporal-network)
;;                                (outfile pathname))
;;   (simple-dot:with-output-to-dot (stream outfile)
;;     (stn-pair-to-dot-stream tn tn2 stream)))

;; (defmethod stn-pair-to-dot-stream ((tn temporal-network) (tn2 temporal-network)
;;                                    &optional (stream *standard-output*)
;;                                    &key (indent 2) (rankdir "LR"))
;;   (multiple-value-bind (apsp-soln start-vertex-name)
;;       (apsp::floyd-warshall-stn tn)
;;     (simple-dot:with-indent-scope (iprint iprintln :default-indent indent :stream stream)
;;       (iprintln "digraph G {")
;;       (simple-dot:with-indent ()
;;         (iprintln "rankdir=~a" rankdir)
;;         (iprintln "node [shape = circle];")
;;         (do-events (event tn)
;;           (iprintln "\"~a\" [label = \"~a\", shape = ~a, fixedsize = true, ~
;;                              width = 0.75, height = 0.75]"
;;                     (id event)
;;                     (format nil
;;                             "~a_~2$_~2$"
;;                             (name event)
;;                             ;; (apsp::get-cost apsp-soln start-vertex-name (tn:id event))
;;                             (distance (itc-ncd-graph (tn:default-consistency-checker tn))
;;                                       (tn:id event))
;;                             (distance (itc-ncd-graph (tn:default-consistency-checker tn2))
;;                                       (tn:id event)))
;;                     ;; (format nil "~a" (name event))
;;                     "circle"))
;;         (do-constraints (constraint tn)
;;           (iprint "\"~a\" -> \"~a\""
;;                   (id (from-event constraint))
;;                   (id (to-event constraint)))
;;           (iprint " [label = \"")
;;           ;; print simple temporal constraint
;;           (when (typep constraint 'simple-temporal-constraint)
;;             ;; (format t "[~a,~a]~%" (lower-bound constraint) (upper-bound constraint))
;;             (iprint "[~a,~a]"
;;                     ;; (get-pretty-number (if (null (lower-bound constraint))
;;                     ;;                        0 (lower-bound constraint)))
;;                     ;; (get-pretty-number (if (null (upper-bound constraint))
;;                     ;;                        +inf+ (upper-bound constraint)))
;;                     (get-pretty-number (lower-bound constraint))
;;                     (get-pretty-number (upper-bound constraint))))
;;           (iprint "\"")
;;           (iprintln "]")))
;;       (iprintln "}"))))

;; (mtk:load-system 'apsp)
;; (in-package :apsp)

;; (defmethod floyd-warshall-stn ((tn tn:temporal-network))
;;   (let ((soln (apsp:make-apsp-full-named (+ 1 (hash-table-size (tn::events-by-id tn)))))
;;         (start-vertex-name (gensym "START-")))
;;     ;; number the IDs
;;     (apsp:set-vertex soln 0 start-vertex-name)
;;     (let ((count 1))
;;       (tn:do-events (event tn)
;;         (set-vertex soln count (tn:id event))
;;         (incf count)))
;;     ;; Build the APSP structure
;;     (tn:do-events (event tn)
;;       (set-cost soln start-vertex-name (tn:id event) 0))
;;     (tn:do-constraints (constraint tn)
;;       (let ((from-id (tn:id (tn:from-event constraint)))
;;             (to-id (tn:id (tn:to-event constraint))))
;;         (when (typep constraint 'tn:simple-temporal-constraint)
;;           (set-cost soln from-id to-id (tn:upper-bound constraint))
;;           (set-cost soln to-id from-id (- (tn:lower-bound constraint))))))
;;     (values (floyd-warshall soln) start-vertex-name)))
