;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :itc-v2/tests)

(in-suite :itc-ncd)

(def-test ncd-two-event-test-duplicate-constraints ()
  ;; Every two event stn is consistent.
  (with-fixture itc-and-stn ()
    (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :e1 :to-event :e2
                   :lower-bound 1 :upper-bound 2)
    (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :e1 :to-event :e2
                   :lower-bound 1 :upper-bound 2)
    (is (temporal-network-consistent? *stn*))))

(def-test ncd-two-event-test ()
  ;; Every two event stn is consistent.
  (for-all ((lb (gen-integer :min -500 :max 500))
            (ub (gen-integer :min -500 :max 500)
                (> ub lb)))
    (with-fixture itc-and-stn ()
      (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :e1 :to-event :e2
                   :lower-bound lb :upper-bound ub)
      (is (temporal-network-consistent? *stn*)))))

(def-test ncd-three-event-test ()
  (for-all ((lb (gen-integer :min 0 :max 50))
            (ub (gen-integer :min 0 :max 50)
                (> ub lb)))
    ;; Everything in this range should be feasible.
    (with-fixture itc-and-stn ()
      (make-instance 'tn:simple-temporal-constraint :network *stn*
                     :from-event :e1 :to-event :e3
                     :lower-bound 0 :upper-bound 50)
      (make-instance 'tn:simple-temporal-constraint :network *stn*
                     :from-event :e1 :to-event :e2
                     :lower-bound 0 :upper-bound 0)
      (make-instance 'tn:simple-temporal-constraint :network *stn*
                     :from-event :e2 :to-event :e3
                     :lower-bound lb :upper-bound ub)
      (is (temporal-network-consistent? *stn*))))
  (for-all ((lb (gen-integer :max -1))
            (ub (gen-integer :max -1)))
    ;; Everything in this range should not be feasible.
    (with-fixture empty-stn ()
      (make-instance 'tn:simple-temporal-constraint :network *stn*
                     :from-event :e1 :to-event :e3
                     :lower-bound 0 :upper-bound 50)
      (make-instance 'tn:simple-temporal-constraint :network *stn*
                     :from-event :e1 :to-event :e2
                     :lower-bound 0 :upper-bound 0)
      (make-instance 'tn:simple-temporal-constraint :network *stn*
                     :from-event :e2 :to-event :e3
                     :lower-bound lb :upper-bound ub)
      (setf (default-consistency-checker *stn*) 'itc-negative-cycle-detection)
      ;; INITIALIZE! can come after any constraints.
      (is (not (temporal-network-consistent? *stn*)))))
  (for-all ((lb (gen-integer :min 51))
            (ub (gen-integer :min 51)))
    ;; Everything in this range should not be feasible.
    (with-fixture empty-stn ()
      (make-instance 'tn:simple-temporal-constraint :network *stn*
                     :from-event :e1 :to-event :e3
                     :lower-bound 0 :upper-bound 50)
      (make-instance 'tn:simple-temporal-constraint :network *stn*
                     :from-event :e1 :to-event :e2
                     :lower-bound 0 :upper-bound 0)
      (setf (default-consistency-checker *stn*) 'itc-negative-cycle-detection)
      (make-instance 'tn:simple-temporal-constraint :network *stn*
                     :from-event :e2 :to-event :e3
                     :lower-bound lb :upper-bound ub)
      (is (not (temporal-network-consistent? *stn*))))))

(def-test ncd-double-consistency-calls ()
  (with-fixture itc-and-stn ()
    ;; Add our starting set of constraints.
    (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :e1 :to-event :e3
                   :lower-bound 0 :upper-bound 50)
    (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :e1 :to-event :e2
                   :lower-bound 0 :upper-bound 0)
    (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :e2 :to-event :e3
                   :lower-bound -10 :upper-bound -1)
    ;; At first, this should NOT be consistent.
    (multiple-value-bind (consistent? neg-cycle)
        (temporal-network-consistent? *stn*)
      (is (not consistent?))
      ;; (format t "B: ~a~%" (debug-print (set-b (itc-ncd-graph *stn*)) nil))
      ;; (format t "NEGATIVE-CYCLE: ~s~%" neg-cycle)
      ;; TODO: Explicitly check negative cycle.
      (is (not (null neg-cycle))))
    ;; Test for repeated checking of consistency without modifications.
    ;; (format t "GRAPH2:~%~a~%" (itc-ncd-graph *stn*))
    ;; (debug-print-vertex-map (itc-ncd-graph *stn*))
    (multiple-value-bind (consistent? neg-cycle)
        (temporal-network-consistent? *stn*)
      (is (not consistent?))
      ;; (format t "B: ~a~%" (debug-print (set-b (itc-ncd-graph *stn*)) nil))
      ;; (format t "NEGATIVE-CYCLE: ~s~%" neg-cycle)
      ;; TODO: Explicitly check negative cycle.
      (is (not (null neg-cycle))))))

(def-test ncd-update-bounds-test ()
  (with-fixture itc-and-stn ()
    ;; Add our starting set of constraints
    (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :e1 :to-event :e3
                   :lower-bound 0 :upper-bound 50)
    (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :e1 :to-event :e2
                   :lower-bound 0 :upper-bound 0)
    (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :e2 :to-event :e3
                   :lower-bound -10 :upper-bound -1
                   :id :test-constraint)
    ;; At first, this should NOT be consistent.
    ;; (format t "GRAPH1:~%~a~%" (itc-ncd-graph *stn*))
    ;; (debug-print-vertex-map (itc-ncd-graph *stn*))
    (multiple-value-bind (consistent? neg-cycle)
        (temporal-network-consistent? *stn*)
      (is (not consistent?))
      ;; (format t "B: ~a~%" (debug-print (set-b (itc-ncd-graph *stn*)) nil))
      ;; (format t "NEGATIVE-CYCLE: ~s~%" neg-cycle)
      ;; TODO: Explicitly check negative cycle.
      (is (not (null neg-cycle))))
    ;; Test for repeated checking of consistency without modifications.
    ;; (format t "GRAPH2:~%~a~%" (itc-ncd-graph *stn*))
    ;; (debug-print-vertex-map (itc-ncd-graph *stn*))
    (multiple-value-bind (consistent? neg-cycle)
        (temporal-network-consistent? *stn*)
      (is (not consistent?))
      ;; (format t "B: ~a~%" (debug-print (set-b (itc-ncd-graph *stn*)) nil))
      ;; (format t "NEGATIVE-CYCLE: ~s~%" neg-cycle)
      ;; TODO: Explicitly check negative cycle.
      (is (not (null neg-cycle))))
    ;; Then change the upper bound of the test-constraint to make it consistent.
    (setf (tn:upper-bound (find-temporal-constraint *stn* :test-constraint)) 25)
    ;; (format t "GRAPH3:~%~a~%" (itc-ncd-graph *stn*))
    ;; (debug-print-vertex-map (itc-ncd-graph *stn*))
    (multiple-value-bind (consistent? neg-cycle)
        (temporal-network-consistent? *stn*)
      (is (eq consistent? t))
      ;; (format t "B: ~a~%" (debug-print (set-b (itc-ncd-graph *stn*)) nil))
      ;; (format t "NEGATIVE-CYCLE: ~s~%" neg-cycle)
      ;; TODO: Explicitly check negative cycle.
      (is (null neg-cycle)))
    ;; Test for repeated checking of consistency without modifications.
    ;; (format t "GRAPH3:~%~a~%" (itc-ncd-graph *stn*))
    ;; (debug-print-vertex-map (itc-ncd-graph *stn*))
    (multiple-value-bind (consistent? neg-cycle)
        (temporal-network-consistent? *stn*)
      (is (eq consistent? t))
      ;; (format t "B: ~a~%" (debug-print (set-b (itc-ncd-graph *stn*)) nil))
      ;; (format t "NEGATIVE-CYCLE: ~s~%" neg-cycle)
      ;; TODO: Explicitly check negative cycle.
      (is (null neg-cycle)))
    ;; Now shift the entire constraint up so that it's inconsistent.
    (setf (tn:lower-bound (find-temporal-constraint *stn* :test-constraint)) 100)
    (setf (tn:upper-bound (find-temporal-constraint *stn* :test-constraint)) 150)
    ;; (format t "GRAPH4:~%~a~%" (itc-ncd-graph *stn*))
    ;; (debug-print-vertex-map (itc-ncd-graph *stn*))
    (multiple-value-bind (consistent? neg-cycle)
        (temporal-network-consistent? *stn*)
      (is (not consistent?))
      ;; (format t "B: ~a~%" (debug-print (set-b (itc-ncd-graph *stn*)) nil))
      ;; (format t "NEGATIVE-CYCLE: ~s~%" neg-cycle)
      ;; TODO: Explicitly check negative cycle.
      (is (not (null neg-cycle))))))

(def-test ncd-multiple-constraints-same-event-pair ()
  (with-fixture itc-and-stn ()
    (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :e1 :to-event :e2
                   :lower-bound 1 :upper-bound 100)
    (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :e1 :to-event :e2
                   :lower-bound -100 :upper-bound -10)
    ;; This should be inconsistent.
    (is (not (temporal-network-consistent? *stn*)))
    ;; Add a third, just to be sure.
    (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :e1 :to-event :e2
                   :lower-bound -150 :upper-bound 200)
    (is (not (temporal-network-consistent? *stn*)))))

(def-test ncd-self-loop ()
  (with-fixture itc-and-stn ()
    (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :e1 :to-event :e1
                   :lower-bound 0 :upper-bound 0)
    ;; This should be consistent.
    (is (temporal-network-consistent? *stn*)))
  (with-fixture itc-and-stn ()
    (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :e1 :to-event :e1
                   :lower-bound 10 :upper-bound 100)
    ;; This should certainly not be consistent.
    (is (not (temporal-network-consistent? *stn*)))))

(def-test ncd-fully-connected-graph ()
  (with-fixture itc-and-stn ()
    (let ((lb 5)
          (ub 8))
      ;; Test a graph where e1 has edges going to e2-e5.
      (dolist (event1 '(:e1))
        (dolist (event2 '(:e2 :e3 :e4 :e5))
          (make-instance 'tn:simple-temporal-constraint :network *stn*
                         :from-event event1 :to-event event2
                         :lower-bound lb :upper-bound ub)
          (is (temporal-network-consistent? *stn*))))
      ;; Add edges to e1 coming from e2-e5.
      (dolist (event1 '(:e1))
        (dolist (event2 '(:e2 :e3 :e4 :e5))
          (make-instance 'tn:simple-temporal-constraint :network *stn*
                         :from-event event2 :to-event event1
                         :lower-bound lb :upper-bound ub)
          (is (not (temporal-network-consistent? *stn*)))))
      ;; Add edges to make the graph fully connected.
      (dolist (event1 '(:e2 :e3 :e4 :e5))
        (dolist (event2 '(:e2 :e3 :e4 :e5))
          (unless (eq event1 event2)
            (make-instance 'tn:simple-temporal-constraint :network *stn*
                           :from-event event1 :to-event event2
                           :lower-bound lb :upper-bound ub)
            (is (not (temporal-network-consistent? *stn*)))))))))

(def-test ncd-double-negative-cycle ()
  (with-fixture itc-and-stn ()
    (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :a :to-event :b
                   :lower-bound +-inf+ :upper-bound 1)
    (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :b :to-event :c
                   :lower-bound +-inf+ :upper-bound 1)
    (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :c :to-event :d
                   :lower-bound +-inf+ :upper-bound 2)
    (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :d :to-event :b
                   :lower-bound +-inf+ :upper-bound -3
                   :id :neg-cycle-1)
    (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :a :to-event :e
                   :lower-bound +-inf+ :upper-bound 1)
    (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :e :to-event :f
                   :lower-bound +-inf+ :upper-bound 1)
    (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :f :to-event :g
                   :lower-bound +-inf+ :upper-bound 3)
    (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :g :to-event :e
                   :lower-bound +-inf+ :upper-bound -4
                   :id :neg-cycle-2)
    (is (temporal-network-consistent? *stn*))

    (setf (tn:upper-bound (find-temporal-constraint *stn* :neg-cycle-1)) -4)
    (is (not (temporal-network-consistent? *stn*)))

    (setf (tn:upper-bound (find-temporal-constraint *stn* :neg-cycle-2)) -5)
    (is (not (temporal-network-consistent? *stn*)))

    (setf (tn:upper-bound (find-temporal-constraint *stn* :neg-cycle-2)) -4)
    (is (not (temporal-network-consistent? *stn*)))

    (setf (tn:upper-bound (find-temporal-constraint *stn* :neg-cycle-1)) -3)
    (is (temporal-network-consistent? *stn*))))
