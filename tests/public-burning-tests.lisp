;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :itc-v2/tests)

(in-suite :itc)

(def-test cold-test ()
  ;; A very simple and light loaded test
  (let* ((stn (make-simple-temporal-network :stress-test))
         (ncycle nil))
    (setf (default-consistency-checker stn) 'itc-negative-cycle-detection)
    (make-instance 'tn:simple-temporal-constraint :network stn
                   :from-event :e1 :to-event :e2
                   :lower-bound 10 :upper-bound 20)
    (make-instance 'tn:simple-temporal-constraint :network stn
                   :from-event :e2 :to-event :e3
                   :lower-bound 10 :upper-bound 20)
    (make-instance 'tn:simple-temporal-constraint :network stn
                   :from-event :e1 :to-event :e2
                   :lower-bound 50 :upper-bound 10)
    (setf ncycle (multiple-value-list (temporal-network-consistent? stn)))
    ;; (format t "Ncycle: ~A~%" ncycle)
    ;; TODO: Explicitly check negative cycle.
    (is (not (null ncycle)))))

(def-test warm-test ()
  ;; A simple test with considerable amount of constraints
  (let* ((stn (make-simple-temporal-network :stress-test))
         (events (list :e1 :e2 :e3 :e4 :e5))
         (lb 5)
         (ub 10)
         (counter 0))
    (setf (default-consistency-checker stn) 'itc-negative-cycle-detection)
    (loop for start-event in events do
         (loop for end-event in events do
              ;; (format t "start: ~a  end: ~a~%" start-event end-event)
              (when (not (equal start-event end-event))
                (make-instance 'tn:simple-temporal-constraint :network stn
                               :from-event start-event :to-event end-event
                               :lower-bound lb :upper-bound ub)
                (setf counter (+ counter 1))
                (if (<= counter 4)
                    (is (temporal-network-consistent? stn))
                    (is-false (temporal-network-consistent? stn)
                              "Counter: ~a. Start ~a. End ~a ."
                              counter start-event end-event)))))))
