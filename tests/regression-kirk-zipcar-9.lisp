(in-package :itc-v2/tests/regression)

(defconstant +inf+ float-features:double-float-positive-infinity)
(defconstant +-inf+ float-features:double-float-negative-infinity)

(in-suite :itc-regressions)

(def-test kirk-zipcar-9 ()
  (let ((stn (tn:make-simple-temporal-network (gensym))))
    (flet ((add-constraint (from-event to-event ub lb id)
             (make-instance 'temporal-networks:simple-temporal-constraint
                            :network stn :id id
                            :from-event from-event :to-event to-event
                            :upper-bound ub :lower-bound lb))
           (remove-constraint (id)
             (temporal-networks:remove-temporal-constraint! stn id))
           (test-consistency (dotfilename)
             (let* ((bfm-graph (itc-ncd-graph (tn:default-consistency-checker stn)))
                    (set-a-before (tolist (set-a bfm-graph)))
                    (set-b-before (tolist (set-b bfm-graph))))
               (multiple-value-bind (consistent? neg-cycle)
                   (temporal-networks:temporal-network-consistent? stn)
                 (dot-print-stn stn
                                (asdf:system-relative-pathname
                                 'itc-v2 (format nil "tmp/~a" dotfilename))
                                :negcycle neg-cycle
                                :set-a-before set-a-before
                                :set-b-before set-b-before)
                 (values consistent? neg-cycle))))
           (dot-stn (dotfilename)
             (let ((bfm-graph (itc-ncd-graph (tn:default-consistency-checker stn))))
               (dot-print-stn stn
                              (asdf:system-relative-pathname
                               'itc-v2 (format nil "tmp/~a" dotfilename))
                              :negcycle nil
                              :set-a-before (tolist (set-a bfm-graph))
                              :set-b-before (tolist (set-b bfm-graph))))))
    (setf (tn:default-consistency-checker stn) 'itc-v2:itc-negative-cycle-detection)

    (add-constraint :|e0| :|e1| 234.9 0.0 :constraint43)
    (add-constraint :|e0| :|e2| 78.54 56.46 :constraint44)
    (add-constraint :|e3| :|e4| 54.38 39.37 :constraint45)
    (add-constraint :|e4| :|e5| 2.69 0.0 :constraint46)
    (add-constraint :|e2| :|e3| 57.9771 40.95 :constraint47)
    (add-constraint :|e6| :|e7| 1.46 0.0 :constraint48)
    (add-constraint :|e8| :|e9| 32.21 23.17 :constraint49)
    (add-constraint :|e10| :|e6| 26.37 18.98 :constraint50)
    (add-constraint :|e9| :|e10| 28.1394 3.15 :constraint51)
    (add-constraint :|e5| :|e11| 42.21 30.83 :constraint52)
    (add-constraint :|e12| :|e13| 12.34 8.86 :constraint53)
    (add-constraint :|e11| :|e12| 20.30805 7.11 :constraint54)
    (add-constraint :|e13| :|e14| 8.36 0.0 :constraint55)
    (add-constraint :|e15| :|e16| 71.51 51.58 :constraint56)
    (add-constraint :|e17| :|e15| 59.8455 20.25 :constraint57)
    (add-constraint :|e16| :|e8| 9.08 0.0 :constraint58)
    (add-constraint :|e14| :|e17| 23.65 16.89 :constraint59)
    (add-constraint :|e18| :|e19| 76.40298 76.41 :constraint60)
    (add-constraint :|e20| :|e21| 0.89 0.0 :constraint61)
    (add-constraint :|e19| :|e20| 68.84 49.75 :constraint62)
    (add-constraint :|e7| :|e18| 55.67 39.85 :constraint63)
    (add-constraint :|e22| :|e1| 9.7 0.0 :constraint64)
    (add-constraint :|e23| :|e22| 44.28 31.63 :constraint65)
    (add-constraint :|e24| :|e23| 85.96854 54.09 :constraint66)
    (add-constraint :|e21| :|e24| 61.06 43.99 :constraint67)
    (is-false (test-consistency "zipcar-test1.dot"))

    (remove-constraint :constraint59)
    (remove-constraint :constraint58)
    (remove-constraint :constraint57)
    (remove-constraint :constraint56)
    (add-constraint :|e14| :|e25| 35.1 25.63 :constraint68)
    (add-constraint :|e26| :|e8| 9.36 0.0 :constraint69)
    (add-constraint :|e27| :|e26| 55.78 40.34 :constraint70)
    (add-constraint :|e25| :|e27| 78.9093 70.65 :constraint71)
    (is-false (test-consistency "zipcar-test2.dot"))

    (remove-constraint :constraint71)
    (remove-constraint :constraint70)
    (remove-constraint :constraint69)
    (remove-constraint :constraint68)
    (add-constraint :|e28| :|e29| 35.6238 24.03 :constraint72)
    (add-constraint :|e30| :|e8| 9.3 0.0 :constraint73)
    (add-constraint :|e29| :|e30| 53.68 38.51 :constraint74)
    (add-constraint :|e14| :|e28| 16.61 11.86 :constraint75)
    (is-false (test-consistency "zipcar-test3.dot"))

    (remove-constraint :constraint75)
    (remove-constraint :constraint74)
    (remove-constraint :constraint73)
    (remove-constraint :constraint72)
    (add-constraint :|e15| :|e16| 71.51 51.58 :constraint76)
    (add-constraint :|e17| :|e15| 59.8455 20.25 :constraint77)
    (add-constraint :|e16| :|e8| 9.08 0.0 :constraint78)
    (add-constraint :|e14| :|e17| 23.65 16.89 :constraint79)
    (remove-constraint :constraint64)
    (remove-constraint :constraint65)
    (remove-constraint :constraint66)
    (remove-constraint :constraint67)
    (add-constraint :|e31| :|e32| 73.06695 61.29 :constraint80)
    (add-constraint :|e33| :|e1| 2.48 0.0 :constraint81)
    (add-constraint :|e32| :|e33| 35.03 25.02 :constraint82)
    (add-constraint :|e21| :|e31| 51.81 37.38 :constraint83)
    (is-false (test-consistency "zipcar-test4.dot"))

    (remove-constraint :constraint83)
    (remove-constraint :constraint82)
    (remove-constraint :constraint81)
    (remove-constraint :constraint80)
    (add-constraint :|e21| :|e34| 53.74 39.23 :constraint84)
    (add-constraint :|e34| :|e35| 84.94137 77.67 :constraint85)
    (add-constraint :|e35| :|e36| 55.48 40.08 :constraint86)
    (add-constraint :|e36| :|e1| 5.0 0.0 :constraint87)
    (is-false (test-consistency "zipcar-test5.dot"))

    (remove-constraint :constraint52)
    (remove-constraint :constraint53)
    (remove-constraint :constraint54)
    (remove-constraint :constraint55)
    (add-constraint :|e5| :|e37| 39.3 28.08 :constraint88)
    (add-constraint :|e38| :|e39| 38.69 27.9 :constraint89)
    (add-constraint :|e39| :|e14| 1.3 0.0 :constraint90)
    (add-constraint :|e37| :|e38| 28.29456 17.82 :constraint91)
    (remove-constraint :constraint84)
    (remove-constraint :constraint85)
    (remove-constraint :constraint86)
    (remove-constraint :constraint87)
    (add-constraint :|e22| :|e1| 9.7 0.0 :constraint92)
    (add-constraint :|e23| :|e22| 44.28 31.63 :constraint93)
    (add-constraint :|e24| :|e23| 85.96854 54.09 :constraint94)
    (add-constraint :|e21| :|e24| 61.06 43.99 :constraint95)
    (is-false (test-consistency "zipcar-test6.dot"))

    (remove-constraint :constraint88)
    (remove-constraint :constraint89)
    (remove-constraint :constraint90)
    (remove-constraint :constraint91)
    (add-constraint :|e13| :|e14| 8.36 0.0 :constraint96)
    (add-constraint :|e11| :|e12| 20.30805 7.11 :constraint97)
    (add-constraint :|e12| :|e13| 12.34 8.86 :constraint98)
    (add-constraint :|e5| :|e11| 42.21 30.83 :constraint99)
    (remove-constraint :constraint76)
    (remove-constraint :constraint77)
    (remove-constraint :constraint78)
    (remove-constraint :constraint79)
    (add-constraint :|e25| :|e27| 78.9093 70.65 :constraint100)
    (add-constraint :|e27| :|e26| 55.78 40.34 :constraint101)
    (add-constraint :|e26| :|e8| 9.36 0.0 :constraint102)
    (add-constraint :|e14| :|e25| 35.1 25.63 :constraint103)
    (remove-constraint :constraint95)
    (remove-constraint :constraint94)
    (remove-constraint :constraint93)
    (remove-constraint :constraint92)
    (add-constraint :|e21| :|e31| 51.81 37.38 :constraint104)
    (add-constraint :|e32| :|e33| 35.03 25.02 :constraint105)
    (add-constraint :|e33| :|e1| 2.48 0.0 :constraint106)
    (add-constraint :|e31| :|e32| 73.06695 61.29 :constraint107)
    (is-false (test-consistency "zipcar-test7.dot"))

    (remove-constraint :constraint100)
    (remove-constraint :constraint101)
    (remove-constraint :constraint102)
    (remove-constraint :constraint103)
    (add-constraint :|e28| :|e29| 35.6238 24.03 :constraint108)
    (add-constraint :|e30| :|e8| 9.3 0.0 :constraint109)
    (add-constraint :|e29| :|e30| 53.68 38.51 :constraint110)
    (add-constraint :|e14| :|e28| 16.61 11.86 :constraint111)
    (is-false (test-consistency "zipcar-test8.dot"))

    (remove-constraint :constraint111)
    (remove-constraint :constraint110)
    (remove-constraint :constraint109)
    (remove-constraint :constraint108)
    (add-constraint :|e15| :|e16| 71.51 51.58 :constraint112)
    (add-constraint :|e17| :|e15| 59.8455 20.25 :constraint113)
    (add-constraint :|e16| :|e8| 9.08 0.0 :constraint114)
    (add-constraint :|e14| :|e17| 23.65 16.89 :constraint115)
    (remove-constraint :constraint63)
    (remove-constraint :constraint62)
    (remove-constraint :constraint61)
    (remove-constraint :constraint60)
    (add-constraint :|e40| :|e41| 54.58 39.36 :constraint116)
    (add-constraint :|e7| :|e42| 15.57 11.12 :constraint117)
    (add-constraint :|e41| :|e21| 1.98 0.0 :constraint118)
    (add-constraint :|e42| :|e40| 75.57732 40.86 :constraint119)
    (remove-constraint :constraint104)
    (remove-constraint :constraint105)
    (remove-constraint :constraint106)
    (remove-constraint :constraint107)
    (add-constraint :|e22| :|e1| 9.7 0.0 :constraint120)
    (add-constraint :|e23| :|e22| 44.28 31.63 :constraint121)
    (add-constraint :|e24| :|e23| 85.96854 54.09 :constraint122)
    (add-constraint :|e21| :|e24| 61.06 43.99 :constraint123)
    (is-false (test-consistency "zipcar-test9.dot"))

    (remove-constraint :constraint115)
    (remove-constraint :constraint114)
    (remove-constraint :constraint113)
    (remove-constraint :constraint112)
    (add-constraint :|e14| :|e25| 35.1 25.63 :constraint124)
    (add-constraint :|e26| :|e8| 9.36 0.0 :constraint125)
    (add-constraint :|e27| :|e26| 55.78 40.34 :constraint126)
    (add-constraint :|e25| :|e27| 78.9093 70.65 :constraint127)
    (remove-constraint :constraint119)
    (remove-constraint :constraint118)
    (remove-constraint :constraint117)
    (remove-constraint :constraint116)
    (add-constraint :|e7| :|e18| 55.67 39.85 :constraint128)
    (add-constraint :|e19| :|e20| 68.84 49.75 :constraint129)
    (add-constraint :|e20| :|e21| 0.89 0.0 :constraint130)
    (add-constraint :|e18| :|e19| 76.40298 76.41 :constraint131)
    (remove-constraint :constraint123)
    (remove-constraint :constraint122)
    (remove-constraint :constraint121)
    (remove-constraint :constraint120)
    (add-constraint :|e36| :|e1| 5.0 0.0 :constraint132)
    (add-constraint :|e35| :|e36| 55.48 40.08 :constraint133)
    (add-constraint :|e34| :|e35| 84.94137 77.67 :constraint134)
    (add-constraint :|e21| :|e34| 53.74 39.23 :constraint135)
    (is-false (test-consistency "zipcar-test10.dot"))

    (remove-constraint :constraint127)
    (remove-constraint :constraint126)
    (remove-constraint :constraint125)
    (remove-constraint :constraint124)
    (add-constraint :|e14| :|e43| 36.5 26.73 :constraint136)
    (add-constraint :|e44| :|e45| 58.39 42.46 :constraint137)
    (add-constraint :|e43| :|e44| 81.88614 35.19 :constraint138)
    (add-constraint :|e45| :|e8| 7.56 0.0 :constraint139)
    (remove-constraint :constraint132)
    (remove-constraint :constraint133)
    (remove-constraint :constraint134)
    (remove-constraint :constraint135)
    (add-constraint :|e22| :|e1| 9.7 0.0 :constraint140)
    (add-constraint :|e23| :|e22| 44.28 31.63 :constraint141)
    (add-constraint :|e24| :|e23| 85.96854 54.09 :constraint142)
    (add-constraint :|e21| :|e24| 61.06 43.99 :constraint143)
    (is-false (test-consistency "zipcar-test11.dot"))

    (remove-constraint :constraint96)
    (remove-constraint :constraint97)
    (remove-constraint :constraint98)
    (remove-constraint :constraint99)
    (add-constraint :|e46| :|e14| 4.49 0.0 :constraint144)
    (add-constraint :|e5| :|e47| 12.92 9.23 :constraint145)
    (add-constraint :|e47| :|e48| 56.19618 22.86 :constraint146)
    (add-constraint :|e48| :|e46| 50.0 36.26 :constraint147)
    (remove-constraint :constraint139)
    (remove-constraint :constraint138)
    (remove-constraint :constraint137)
    (remove-constraint :constraint136)
    (add-constraint :|e15| :|e16| 71.51 51.58 :constraint148)
    (add-constraint :|e17| :|e15| 59.8455 20.25 :constraint149)
    (add-constraint :|e16| :|e8| 9.08 0.0 :constraint150)
    (add-constraint :|e14| :|e17| 23.65 16.89 :constraint151)
    (is-false (test-consistency "zipcar-test12.dot"))

    (remove-constraint :constraint144)
    (remove-constraint :constraint145)
    (remove-constraint :constraint146)
    (remove-constraint :constraint147)
    (add-constraint :|e49| :|e50| 22.02 15.73 :constraint152)
    (add-constraint :|e5| :|e51| 51.88 37.7 :constraint153)
    (add-constraint :|e51| :|e49| 37.49256 35.82 :constraint154)
    (add-constraint :|e50| :|e14| 4.7 0.0 :constraint155)
    (is-false (test-consistency "zipcar-test13.dot"))

    (remove-constraint :constraint155)
    (remove-constraint :constraint154)
    (remove-constraint :constraint153)
    (remove-constraint :constraint152)
    (add-constraint :|e5| :|e37| 39.3 28.08 :constraint156)
    (add-constraint :|e38| :|e39| 38.69 27.9 :constraint157)
    (add-constraint :|e39| :|e14| 1.3 0.0 :constraint158)
    (add-constraint :|e37| :|e38| 28.29456 17.82 :constraint159)
    (remove-constraint :constraint151)
    (remove-constraint :constraint150)
    (remove-constraint :constraint149)
    (remove-constraint :constraint148)
    (add-constraint :|e14| :|e25| 35.1 25.63 :constraint160)
    (add-constraint :|e26| :|e8| 9.36 0.0 :constraint161)
    (add-constraint :|e27| :|e26| 55.78 40.34 :constraint162)
    (add-constraint :|e25| :|e27| 78.9093 70.65 :constraint163)
    (is-false (test-consistency "zipcar-test14.dot"))

    (remove-constraint :constraint156)
    (remove-constraint :constraint157)
    (remove-constraint :constraint158)
    (remove-constraint :constraint159)
    (add-constraint :|e13| :|e14| 8.36 0.0 :constraint164)
    (add-constraint :|e11| :|e12| 20.30805 7.11 :constraint165)
    (add-constraint :|e12| :|e13| 12.34 8.86 :constraint166)
    (add-constraint :|e5| :|e11| 42.21 30.83 :constraint167)
    (remove-constraint :constraint160)
    (remove-constraint :constraint161)
    (remove-constraint :constraint162)
    (remove-constraint :constraint163)
    (add-constraint :|e28| :|e29| 35.6238 24.03 :constraint168)
    (add-constraint :|e30| :|e8| 9.3 0.0 :constraint169)
    (add-constraint :|e29| :|e30| 53.68 38.51 :constraint170)
    (add-constraint :|e14| :|e28| 16.61 11.86 :constraint171)
    (remove-constraint :constraint140)
    (remove-constraint :constraint141)
    (remove-constraint :constraint142)
    (remove-constraint :constraint143)
    (add-constraint :|e21| :|e34| 53.74 39.23 :constraint172)
    (add-constraint :|e34| :|e35| 84.94137 77.67 :constraint173)
    (add-constraint :|e35| :|e36| 55.48 40.08 :constraint174)
    (add-constraint :|e36| :|e1| 5.0 0.0 :constraint175)
    (is-false (test-consistency "zipcar-test15.dot"))

    (remove-constraint :constraint167)
    (remove-constraint :constraint166)
    (remove-constraint :constraint165)
    (remove-constraint :constraint164)
    (add-constraint :|e5| :|e37| 39.3 28.08 :constraint176)
    (add-constraint :|e38| :|e39| 38.69 27.9 :constraint177)
    (add-constraint :|e39| :|e14| 1.3 0.0 :constraint178)
    (add-constraint :|e37| :|e38| 28.29456 17.82 :constraint179)
    (remove-constraint :constraint172)
    (remove-constraint :constraint173)
    (remove-constraint :constraint174)
    (remove-constraint :constraint175)
    (add-constraint :|e21| :|e24| 61.06 43.99 :constraint180)
    (add-constraint :|e24| :|e23| 85.96854 54.09 :constraint181)
    (add-constraint :|e23| :|e22| 44.28 31.63 :constraint182)
    (add-constraint :|e22| :|e1| 9.7 0.0 :constraint183)
    (is-false (test-consistency "zipcar-test16.dot"))

    (remove-constraint :constraint176)
    (remove-constraint :constraint177)
    (remove-constraint :constraint178)
    (remove-constraint :constraint179)
    (add-constraint :|e13| :|e14| 8.36 0.0 :constraint184)
    (add-constraint :|e11| :|e12| 20.30805 7.11 :constraint185)
    (add-constraint :|e12| :|e13| 12.34 8.86 :constraint186)
    (add-constraint :|e5| :|e11| 42.21 30.83 :constraint187)
    (remove-constraint :constraint171)
    (remove-constraint :constraint170)
    (remove-constraint :constraint169)
    (remove-constraint :constraint168)
    (add-constraint :|e14| :|e25| 35.1 25.63 :constraint188)
    (add-constraint :|e26| :|e8| 9.36 0.0 :constraint189)
    (add-constraint :|e27| :|e26| 55.78 40.34 :constraint190)
    (add-constraint :|e25| :|e27| 78.9093 70.65 :constraint191)
    (remove-constraint :constraint128)
    (remove-constraint :constraint129)
    (remove-constraint :constraint130)
    (remove-constraint :constraint131)
    (add-constraint :|e40| :|e41| 54.58 39.36 :constraint192)
    (add-constraint :|e7| :|e42| 15.57 11.12 :constraint193)
    (add-constraint :|e41| :|e21| 1.98 0.0 :constraint194)
    (add-constraint :|e42| :|e40| 75.57732 40.86 :constraint195)
    (is-false (test-consistency "test17.dot"))

    ;; reinitialize itc.
    (setf (tn:default-consistency-checker stn) 'itc-v2:itc-negative-cycle-detection)
    (is-false (test-consistency "test18.dot")))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; The following code was used to textually convert the original names of
;; the events in the above zipcar test to shorter, legible names for debugging.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar *symbol-list* '(:|id-d62231ea-e482-44cc-9551-bd907d8f3b80|
:|id-dbfb5fcc-1651-4125-ae37-4b9fe4088a66|
:|id-d62231ea-e482-44cc-9551-bd907d8f3b80|
:|id-fe8e9a13-6e1d-4816-857a-140cc8f1e680|
:|id-215753e5-1c58-4045-8ce4-8c4256f2c10a|
:|id-ffe21125-2c41-48a2-9f49-3417257bbb6f|
:|id-ffe21125-2c41-48a2-9f49-3417257bbb6f|
:|id-ab3dcb89-913a-4c09-a68e-ee4b47d6c678|
:|id-fe8e9a13-6e1d-4816-857a-140cc8f1e680|
:|id-215753e5-1c58-4045-8ce4-8c4256f2c10a|
:|id-f58388fa-287b-4899-b91b-ef6b577ede93|
:|id-1ff73f81-cc26-4d52-858d-19fd4cf2928f|
:|id-538c0d09-f6cf-48c9-b180-1372ccf41ebe|
:|id-382a3ad7-392d-4a03-84fd-88635d60b9f5|
:|id-5efba0ed-9bd7-4b76-8565-5a8ca6d27c2d|
:|id-f58388fa-287b-4899-b91b-ef6b577ede93|
:|id-382a3ad7-392d-4a03-84fd-88635d60b9f5|
:|id-5efba0ed-9bd7-4b76-8565-5a8ca6d27c2d|
:|id-ab3dcb89-913a-4c09-a68e-ee4b47d6c678|
:|id-84680db7-e371-454d-9c4d-5797ee9a2346|
:|id-48494290-ff27-4f3c-bfe1-8c2c9f185c13|
:|id-d22d45b3-7004-470b-9e5f-55fbad015300|
:|id-84680db7-e371-454d-9c4d-5797ee9a2346|
:|id-48494290-ff27-4f3c-bfe1-8c2c9f185c13|
:|id-d22d45b3-7004-470b-9e5f-55fbad015300|
:|id-fb5b9f12-fd4a-4f69-b3da-38b8d4e5c66f|
:|id-32c13c22-393f-48cc-ab4d-d8110238da22|
:|id-b26301a3-4b23-4419-9ce7-d904f98c4bdc|
:|id-6bfb0fad-6bc1-4cf3-a908-ce3c92ae45c3|
:|id-32c13c22-393f-48cc-ab4d-d8110238da22|
:|id-b26301a3-4b23-4419-9ce7-d904f98c4bdc|
:|id-538c0d09-f6cf-48c9-b180-1372ccf41ebe|
:|id-fb5b9f12-fd4a-4f69-b3da-38b8d4e5c66f|
:|id-6bfb0fad-6bc1-4cf3-a908-ce3c92ae45c3|
:|id-f68bf33c-3215-41d0-b098-682de39f5d92|
:|id-76911c87-3b39-497d-8213-dd63e9748e00|
:|id-bec5a17a-291d-42d5-8fad-0d0087d2362d|
:|id-585c9a41-ce9e-49a4-b099-8a2afd414c35|
:|id-76911c87-3b39-497d-8213-dd63e9748e00|
:|id-bec5a17a-291d-42d5-8fad-0d0087d2362d|
:|id-1ff73f81-cc26-4d52-858d-19fd4cf2928f|
:|id-f68bf33c-3215-41d0-b098-682de39f5d92|
:|id-7a081842-39e8-47a1-ac9c-fd200505fe87|
:|id-dbfb5fcc-1651-4125-ae37-4b9fe4088a66|
:|id-83662964-f108-43e7-a5d0-4d9295fbaa42|
:|id-7a081842-39e8-47a1-ac9c-fd200505fe87|
:|id-5e92ee1f-2222-469d-8fab-a52f484fb39f|
:|id-83662964-f108-43e7-a5d0-4d9295fbaa42|
:|id-585c9a41-ce9e-49a4-b099-8a2afd414c35|
:|id-5e92ee1f-2222-469d-8fab-a52f484fb39f|
:|id-fb5b9f12-fd4a-4f69-b3da-38b8d4e5c66f|
:|id-153e08cf-fc61-4947-b731-3dde792b8f75|
:|id-5adc3b90-0571-48f1-a964-bc6ac2afdb61|
:|id-538c0d09-f6cf-48c9-b180-1372ccf41ebe|
:|id-b9e9e24e-31f1-4726-860e-227e5879b904|
:|id-5adc3b90-0571-48f1-a964-bc6ac2afdb61|
:|id-153e08cf-fc61-4947-b731-3dde792b8f75|
:|id-b9e9e24e-31f1-4726-860e-227e5879b904|
:|id-5c9cbde5-b949-4a8b-b474-93d5da90a22d|
:|id-a43201af-3f3e-431f-8d55-ca2dfa39e4e2|
:|id-f6f22fdd-d7cd-4aed-adbd-769d410f51d7|
:|id-538c0d09-f6cf-48c9-b180-1372ccf41ebe|
:|id-a43201af-3f3e-431f-8d55-ca2dfa39e4e2|
:|id-f6f22fdd-d7cd-4aed-adbd-769d410f51d7|
:|id-fb5b9f12-fd4a-4f69-b3da-38b8d4e5c66f|
:|id-5c9cbde5-b949-4a8b-b474-93d5da90a22d|
:|id-32c13c22-393f-48cc-ab4d-d8110238da22|
:|id-b26301a3-4b23-4419-9ce7-d904f98c4bdc|
:|id-6bfb0fad-6bc1-4cf3-a908-ce3c92ae45c3|
:|id-32c13c22-393f-48cc-ab4d-d8110238da22|
:|id-b26301a3-4b23-4419-9ce7-d904f98c4bdc|
:|id-538c0d09-f6cf-48c9-b180-1372ccf41ebe|
:|id-fb5b9f12-fd4a-4f69-b3da-38b8d4e5c66f|
:|id-6bfb0fad-6bc1-4cf3-a908-ce3c92ae45c3|
:|id-a14cfb6a-cffc-4a54-a13b-a89e3c72082c|
:|id-5da5a9a5-c831-43f6-aab6-a4b889211138|
:|id-701d06ac-2b1f-4a8c-ad95-03395c17addf|
:|id-dbfb5fcc-1651-4125-ae37-4b9fe4088a66|
:|id-5da5a9a5-c831-43f6-aab6-a4b889211138|
:|id-701d06ac-2b1f-4a8c-ad95-03395c17addf|
:|id-585c9a41-ce9e-49a4-b099-8a2afd414c35|
:|id-a14cfb6a-cffc-4a54-a13b-a89e3c72082c|
:|id-585c9a41-ce9e-49a4-b099-8a2afd414c35|
:|id-86001cf2-4f07-41ea-8c67-528b3527ba20|
:|id-86001cf2-4f07-41ea-8c67-528b3527ba20|
:|id-eff184c3-76a6-416b-ad29-122ecb291c26|
:|id-eff184c3-76a6-416b-ad29-122ecb291c26|
:|id-17b17f3b-feb4-4226-805e-2467592bfc29|
:|id-17b17f3b-feb4-4226-805e-2467592bfc29|
:|id-dbfb5fcc-1651-4125-ae37-4b9fe4088a66|
:|id-ab3dcb89-913a-4c09-a68e-ee4b47d6c678|
:|id-11ddc01e-0761-4b14-87f5-2a67c9112eb5|
:|id-957f7949-b2ee-4191-9793-0e3aab2c1688|
:|id-a09134a1-e262-4a4e-a369-90af134c0902|
:|id-a09134a1-e262-4a4e-a369-90af134c0902|
:|id-fb5b9f12-fd4a-4f69-b3da-38b8d4e5c66f|
:|id-11ddc01e-0761-4b14-87f5-2a67c9112eb5|
:|id-957f7949-b2ee-4191-9793-0e3aab2c1688|
:|id-7a081842-39e8-47a1-ac9c-fd200505fe87|
:|id-dbfb5fcc-1651-4125-ae37-4b9fe4088a66|
:|id-83662964-f108-43e7-a5d0-4d9295fbaa42|
:|id-7a081842-39e8-47a1-ac9c-fd200505fe87|
:|id-5e92ee1f-2222-469d-8fab-a52f484fb39f|
:|id-83662964-f108-43e7-a5d0-4d9295fbaa42|
:|id-585c9a41-ce9e-49a4-b099-8a2afd414c35|
:|id-5e92ee1f-2222-469d-8fab-a52f484fb39f|
:|id-d22d45b3-7004-470b-9e5f-55fbad015300|
:|id-fb5b9f12-fd4a-4f69-b3da-38b8d4e5c66f|
:|id-84680db7-e371-454d-9c4d-5797ee9a2346|
:|id-48494290-ff27-4f3c-bfe1-8c2c9f185c13|
:|id-48494290-ff27-4f3c-bfe1-8c2c9f185c13|
:|id-d22d45b3-7004-470b-9e5f-55fbad015300|
:|id-ab3dcb89-913a-4c09-a68e-ee4b47d6c678|
:|id-84680db7-e371-454d-9c4d-5797ee9a2346|
:|id-153e08cf-fc61-4947-b731-3dde792b8f75|
:|id-b9e9e24e-31f1-4726-860e-227e5879b904|
:|id-b9e9e24e-31f1-4726-860e-227e5879b904|
:|id-5adc3b90-0571-48f1-a964-bc6ac2afdb61|
:|id-5adc3b90-0571-48f1-a964-bc6ac2afdb61|
:|id-538c0d09-f6cf-48c9-b180-1372ccf41ebe|
:|id-fb5b9f12-fd4a-4f69-b3da-38b8d4e5c66f|
:|id-153e08cf-fc61-4947-b731-3dde792b8f75|
:|id-585c9a41-ce9e-49a4-b099-8a2afd414c35|
:|id-a14cfb6a-cffc-4a54-a13b-a89e3c72082c|
:|id-5da5a9a5-c831-43f6-aab6-a4b889211138|
:|id-701d06ac-2b1f-4a8c-ad95-03395c17addf|
:|id-701d06ac-2b1f-4a8c-ad95-03395c17addf|
:|id-dbfb5fcc-1651-4125-ae37-4b9fe4088a66|
:|id-a14cfb6a-cffc-4a54-a13b-a89e3c72082c|
:|id-5da5a9a5-c831-43f6-aab6-a4b889211138|
:|id-5c9cbde5-b949-4a8b-b474-93d5da90a22d|
:|id-a43201af-3f3e-431f-8d55-ca2dfa39e4e2|
:|id-f6f22fdd-d7cd-4aed-adbd-769d410f51d7|
:|id-538c0d09-f6cf-48c9-b180-1372ccf41ebe|
:|id-a43201af-3f3e-431f-8d55-ca2dfa39e4e2|
:|id-f6f22fdd-d7cd-4aed-adbd-769d410f51d7|
:|id-fb5b9f12-fd4a-4f69-b3da-38b8d4e5c66f|
:|id-5c9cbde5-b949-4a8b-b474-93d5da90a22d|
:|id-32c13c22-393f-48cc-ab4d-d8110238da22|
:|id-b26301a3-4b23-4419-9ce7-d904f98c4bdc|
:|id-6bfb0fad-6bc1-4cf3-a908-ce3c92ae45c3|
:|id-32c13c22-393f-48cc-ab4d-d8110238da22|
:|id-b26301a3-4b23-4419-9ce7-d904f98c4bdc|
:|id-538c0d09-f6cf-48c9-b180-1372ccf41ebe|
:|id-fb5b9f12-fd4a-4f69-b3da-38b8d4e5c66f|
:|id-6bfb0fad-6bc1-4cf3-a908-ce3c92ae45c3|
:|id-5122d340-d71e-4e74-9777-aee34716fcac|
:|id-49deb511-c5f7-40c0-a99a-05aec08c6e1d|
:|id-1ff73f81-cc26-4d52-858d-19fd4cf2928f|
:|id-de639d99-08bc-4c52-a355-1addb2a93ced|
:|id-49deb511-c5f7-40c0-a99a-05aec08c6e1d|
:|id-585c9a41-ce9e-49a4-b099-8a2afd414c35|
:|id-de639d99-08bc-4c52-a355-1addb2a93ced|
:|id-5122d340-d71e-4e74-9777-aee34716fcac|
:|id-7a081842-39e8-47a1-ac9c-fd200505fe87|
:|id-dbfb5fcc-1651-4125-ae37-4b9fe4088a66|
:|id-83662964-f108-43e7-a5d0-4d9295fbaa42|
:|id-7a081842-39e8-47a1-ac9c-fd200505fe87|
:|id-5e92ee1f-2222-469d-8fab-a52f484fb39f|
:|id-83662964-f108-43e7-a5d0-4d9295fbaa42|
:|id-585c9a41-ce9e-49a4-b099-8a2afd414c35|
:|id-5e92ee1f-2222-469d-8fab-a52f484fb39f|
:|id-fb5b9f12-fd4a-4f69-b3da-38b8d4e5c66f|
:|id-153e08cf-fc61-4947-b731-3dde792b8f75|
:|id-5adc3b90-0571-48f1-a964-bc6ac2afdb61|
:|id-538c0d09-f6cf-48c9-b180-1372ccf41ebe|
:|id-b9e9e24e-31f1-4726-860e-227e5879b904|
:|id-5adc3b90-0571-48f1-a964-bc6ac2afdb61|
:|id-153e08cf-fc61-4947-b731-3dde792b8f75|
:|id-b9e9e24e-31f1-4726-860e-227e5879b904|
:|id-1ff73f81-cc26-4d52-858d-19fd4cf2928f|
:|id-f68bf33c-3215-41d0-b098-682de39f5d92|
:|id-76911c87-3b39-497d-8213-dd63e9748e00|
:|id-bec5a17a-291d-42d5-8fad-0d0087d2362d|
:|id-bec5a17a-291d-42d5-8fad-0d0087d2362d|
:|id-585c9a41-ce9e-49a4-b099-8a2afd414c35|
:|id-f68bf33c-3215-41d0-b098-682de39f5d92|
:|id-76911c87-3b39-497d-8213-dd63e9748e00|
:|id-17b17f3b-feb4-4226-805e-2467592bfc29|
:|id-dbfb5fcc-1651-4125-ae37-4b9fe4088a66|
:|id-eff184c3-76a6-416b-ad29-122ecb291c26|
:|id-17b17f3b-feb4-4226-805e-2467592bfc29|
:|id-86001cf2-4f07-41ea-8c67-528b3527ba20|
:|id-eff184c3-76a6-416b-ad29-122ecb291c26|
:|id-585c9a41-ce9e-49a4-b099-8a2afd414c35|
:|id-86001cf2-4f07-41ea-8c67-528b3527ba20|
:|id-fb5b9f12-fd4a-4f69-b3da-38b8d4e5c66f|
:|id-cafc2f94-3531-47ee-b58e-75c060d8bc5e|
:|id-b018dfce-028c-4362-a8fc-6b2246d77b6f|
:|id-1946e66e-f9ff-4f8e-806a-2447e639b3d9|
:|id-cafc2f94-3531-47ee-b58e-75c060d8bc5e|
:|id-b018dfce-028c-4362-a8fc-6b2246d77b6f|
:|id-1946e66e-f9ff-4f8e-806a-2447e639b3d9|
:|id-538c0d09-f6cf-48c9-b180-1372ccf41ebe|
:|id-7a081842-39e8-47a1-ac9c-fd200505fe87|
:|id-dbfb5fcc-1651-4125-ae37-4b9fe4088a66|
:|id-83662964-f108-43e7-a5d0-4d9295fbaa42|
:|id-7a081842-39e8-47a1-ac9c-fd200505fe87|
:|id-5e92ee1f-2222-469d-8fab-a52f484fb39f|
:|id-83662964-f108-43e7-a5d0-4d9295fbaa42|
:|id-585c9a41-ce9e-49a4-b099-8a2afd414c35|
:|id-5e92ee1f-2222-469d-8fab-a52f484fb39f|
:|id-028781fb-a7ee-409c-be10-ed782ac19585|
:|id-fb5b9f12-fd4a-4f69-b3da-38b8d4e5c66f|
:|id-ab3dcb89-913a-4c09-a68e-ee4b47d6c678|
:|id-c81200fe-80cb-4c1f-9a3c-40c637962edd|
:|id-c81200fe-80cb-4c1f-9a3c-40c637962edd|
:|id-32ac1629-b729-45f8-aff9-9e40e366cbde|
:|id-32ac1629-b729-45f8-aff9-9e40e366cbde|
:|id-028781fb-a7ee-409c-be10-ed782ac19585|
:|id-32c13c22-393f-48cc-ab4d-d8110238da22|
:|id-b26301a3-4b23-4419-9ce7-d904f98c4bdc|
:|id-6bfb0fad-6bc1-4cf3-a908-ce3c92ae45c3|
:|id-32c13c22-393f-48cc-ab4d-d8110238da22|
:|id-b26301a3-4b23-4419-9ce7-d904f98c4bdc|
:|id-538c0d09-f6cf-48c9-b180-1372ccf41ebe|
:|id-fb5b9f12-fd4a-4f69-b3da-38b8d4e5c66f|
:|id-6bfb0fad-6bc1-4cf3-a908-ce3c92ae45c3|
:|id-afd5bc9f-8056-4f37-9d23-b7e902327477|
:|id-0818d108-2f3f-4716-b4b9-220f0a38ea0a|
:|id-ab3dcb89-913a-4c09-a68e-ee4b47d6c678|
:|id-202639ea-5891-4bec-ae39-3e7b1f12040d|
:|id-202639ea-5891-4bec-ae39-3e7b1f12040d|
:|id-afd5bc9f-8056-4f37-9d23-b7e902327477|
:|id-0818d108-2f3f-4716-b4b9-220f0a38ea0a|
:|id-fb5b9f12-fd4a-4f69-b3da-38b8d4e5c66f|
:|id-ab3dcb89-913a-4c09-a68e-ee4b47d6c678|
:|id-11ddc01e-0761-4b14-87f5-2a67c9112eb5|
:|id-957f7949-b2ee-4191-9793-0e3aab2c1688|
:|id-a09134a1-e262-4a4e-a369-90af134c0902|
:|id-a09134a1-e262-4a4e-a369-90af134c0902|
:|id-fb5b9f12-fd4a-4f69-b3da-38b8d4e5c66f|
:|id-11ddc01e-0761-4b14-87f5-2a67c9112eb5|
:|id-957f7949-b2ee-4191-9793-0e3aab2c1688|
:|id-fb5b9f12-fd4a-4f69-b3da-38b8d4e5c66f|
:|id-153e08cf-fc61-4947-b731-3dde792b8f75|
:|id-5adc3b90-0571-48f1-a964-bc6ac2afdb61|
:|id-538c0d09-f6cf-48c9-b180-1372ccf41ebe|
:|id-b9e9e24e-31f1-4726-860e-227e5879b904|
:|id-5adc3b90-0571-48f1-a964-bc6ac2afdb61|
:|id-153e08cf-fc61-4947-b731-3dde792b8f75|
:|id-b9e9e24e-31f1-4726-860e-227e5879b904|
:|id-d22d45b3-7004-470b-9e5f-55fbad015300|
:|id-fb5b9f12-fd4a-4f69-b3da-38b8d4e5c66f|
:|id-84680db7-e371-454d-9c4d-5797ee9a2346|
:|id-48494290-ff27-4f3c-bfe1-8c2c9f185c13|
:|id-48494290-ff27-4f3c-bfe1-8c2c9f185c13|
:|id-d22d45b3-7004-470b-9e5f-55fbad015300|
:|id-ab3dcb89-913a-4c09-a68e-ee4b47d6c678|
:|id-84680db7-e371-454d-9c4d-5797ee9a2346|
:|id-5c9cbde5-b949-4a8b-b474-93d5da90a22d|
:|id-a43201af-3f3e-431f-8d55-ca2dfa39e4e2|
:|id-f6f22fdd-d7cd-4aed-adbd-769d410f51d7|
:|id-538c0d09-f6cf-48c9-b180-1372ccf41ebe|
:|id-a43201af-3f3e-431f-8d55-ca2dfa39e4e2|
:|id-f6f22fdd-d7cd-4aed-adbd-769d410f51d7|
:|id-fb5b9f12-fd4a-4f69-b3da-38b8d4e5c66f|
:|id-5c9cbde5-b949-4a8b-b474-93d5da90a22d|
:|id-585c9a41-ce9e-49a4-b099-8a2afd414c35|
:|id-86001cf2-4f07-41ea-8c67-528b3527ba20|
:|id-86001cf2-4f07-41ea-8c67-528b3527ba20|
:|id-eff184c3-76a6-416b-ad29-122ecb291c26|
:|id-eff184c3-76a6-416b-ad29-122ecb291c26|
:|id-17b17f3b-feb4-4226-805e-2467592bfc29|
:|id-17b17f3b-feb4-4226-805e-2467592bfc29|
:|id-dbfb5fcc-1651-4125-ae37-4b9fe4088a66|
:|id-ab3dcb89-913a-4c09-a68e-ee4b47d6c678|
:|id-11ddc01e-0761-4b14-87f5-2a67c9112eb5|
:|id-957f7949-b2ee-4191-9793-0e3aab2c1688|
:|id-a09134a1-e262-4a4e-a369-90af134c0902|
:|id-a09134a1-e262-4a4e-a369-90af134c0902|
:|id-fb5b9f12-fd4a-4f69-b3da-38b8d4e5c66f|
:|id-11ddc01e-0761-4b14-87f5-2a67c9112eb5|
:|id-957f7949-b2ee-4191-9793-0e3aab2c1688|
:|id-585c9a41-ce9e-49a4-b099-8a2afd414c35|
:|id-5e92ee1f-2222-469d-8fab-a52f484fb39f|
:|id-5e92ee1f-2222-469d-8fab-a52f484fb39f|
:|id-83662964-f108-43e7-a5d0-4d9295fbaa42|
:|id-83662964-f108-43e7-a5d0-4d9295fbaa42|
:|id-7a081842-39e8-47a1-ac9c-fd200505fe87|
:|id-7a081842-39e8-47a1-ac9c-fd200505fe87|
:|id-dbfb5fcc-1651-4125-ae37-4b9fe4088a66|
:|id-d22d45b3-7004-470b-9e5f-55fbad015300|
:|id-fb5b9f12-fd4a-4f69-b3da-38b8d4e5c66f|
:|id-84680db7-e371-454d-9c4d-5797ee9a2346|
:|id-48494290-ff27-4f3c-bfe1-8c2c9f185c13|
:|id-48494290-ff27-4f3c-bfe1-8c2c9f185c13|
:|id-d22d45b3-7004-470b-9e5f-55fbad015300|
:|id-ab3dcb89-913a-4c09-a68e-ee4b47d6c678|
:|id-84680db7-e371-454d-9c4d-5797ee9a2346|
:|id-fb5b9f12-fd4a-4f69-b3da-38b8d4e5c66f|
:|id-153e08cf-fc61-4947-b731-3dde792b8f75|
:|id-5adc3b90-0571-48f1-a964-bc6ac2afdb61|
:|id-538c0d09-f6cf-48c9-b180-1372ccf41ebe|
:|id-b9e9e24e-31f1-4726-860e-227e5879b904|
:|id-5adc3b90-0571-48f1-a964-bc6ac2afdb61|
:|id-153e08cf-fc61-4947-b731-3dde792b8f75|
:|id-b9e9e24e-31f1-4726-860e-227e5879b904|
:|id-5122d340-d71e-4e74-9777-aee34716fcac|
:|id-49deb511-c5f7-40c0-a99a-05aec08c6e1d|
:|id-1ff73f81-cc26-4d52-858d-19fd4cf2928f|
:|id-de639d99-08bc-4c52-a355-1addb2a93ced|
:|id-49deb511-c5f7-40c0-a99a-05aec08c6e1d|
:|id-585c9a41-ce9e-49a4-b099-8a2afd414c35|
:|id-de639d99-08bc-4c52-a355-1addb2a93ced|
:|id-5122d340-d71e-4e74-9777-aee34716fcac|))

(defun get-unique-id (symbol-list)
  (let ((map (make-hashmap))
        (count 0))
    (dolist (sym symbol-list)
      (when (not (map-contains? map sym))
        (map-add! map sym (intern (format nil "e~a" count)))
        (incf count)))
    map))
