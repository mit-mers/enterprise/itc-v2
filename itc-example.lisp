;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package #:itc-v2)

(defvar *stn* nil "STN object currently being used within a test.")

(defun itc-ncd-example ()
  "This function gives you an example of how to use the Negative Cycle Detection (NCD) variant of ITC on a TEMPORAL-NETWORK object.
   (There is another reference implementation of ITC that uses a Linear Program Solver)."
  (let* ((*stn* (make-simple-temporal-network (gensym "EXAMPLE-TEMPORAL-NETWORK-"))))
    ;; Set the default consistency checker of *STN* to be
    ;; ITC-NEGATIVE-CYCLE-DETECTION. When this SETF method is called
    ;; this way, it automatically creates a new instance of
    ;; 'ITC-NEGATIVE-CYCLE-DETECTION and passes the network as an
    ;; initarg.
    (setf (default-consistency-checker *stn*) 'itc-negative-cycle-detection)

    ;; Now you can modify your temporal network however you want...
    ;; Here, we add three temporal constraints to our STN.
    ;; (Note that you don't have to call any special ITC function)
    (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :e1 :to-event :e3 :lower-bound 0 :upper-bound 50)
    (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :e1 :to-event :e2 :lower-bound 0 :upper-bound 0)
    (make-instance 'tn:simple-temporal-constraint :network *stn*
                   :from-event :e2 :to-event :e3 :lower-bound -10 :upper-bound -1 :id :test-constraint)

    ;; You can test for consistency by simply by calling (temporal-network-consistent? <your-temporal-network-object>)
    ;; The call returns two values:
    ;; * The first value is 't' if your network is consistent. 'nil' otherwise.
    ;; * If the first value is 'nil', the second value will be a list of conflicts of the form:
    ;;   ((EVENT-A-NAME EVENT-B-NAME) (EVENT-B-NAME EVENT-C-NAME) (EVENT-C-NAME EVENT-A-NAME))
    ;;   It is a list of lists, in which each sub-list contains the start event and end event name
    ;;   of the participating temporal constraint.
    (temporal-network-consistent? *stn*)

    ;; At first, the graph we have created is NOT consistent.
    ;; * We can use this fancy multiple-value bind to get and print out the secondary return value.
    ;; * Note that you can call (temporal-network-consistent?) on your temporal network as many times
    ;;    as you'd like. Here we call it again, even though we didn't make any changes to the
    ;;    temporal network object, *stn*.
    (multiple-value-bind (consistent? neg-cycle)
        (temporal-network-consistent? *stn*)
      (format t "Consistency: ~a  Negative-Cycle: ~a~%" consistent? neg-cycle))

    ;; Now let's change the upper bound of the test-constraint to make it consistent.
    (setf (tn:upper-bound (tn:find-temporal-constraint *stn* :test-constraint)) 25)

    ;; Now we should be consistent.
    (multiple-value-bind (consistent? neg-cycle)
        (temporal-network-consistent? *stn*)
      (format t "Consistency: ~a  Negative-Cycle: ~a~%" consistent? neg-cycle))

    ;; Now shift the entire constraint up so that it's inconsistent.
    ;; But, let's do it using the (duration-lower-bound) and (duration-upper-bound) functions.
    (setf (tn:lower-bound (tn:find-temporal-constraint *stn* :test-constraint)) 100)
    (setf (tn:upper-bound (tn:find-temporal-constraint *stn* :test-constraint)) 150)

    ;; Now we're inconsistent again. = (
    (multiple-value-bind (consistent? neg-cycle)
        (temporal-network-consistent? *stn*)
      (format t "Consistency: ~a  Negative-Cycle: ~a~%" consistent? neg-cycle))))
