;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :itc-v2)

(defmethod itc-initialize! ((itc itc-linear-program) &key &allow-other-keys)
  (itc-linear-program-initialize! itc (itc-temporal-network itc)))

(defmethod itc-linear-program-initialize! (itc (network simple-temporal-network))
  "Right now this works only on simple-temporal-networks. Possibly
expand in the future. Create a linear program from all the constraints
in the temporal network."
  ;; If we get here, we can safely assume that ITC is an instance of
  ;; ITC-LINEAR-PROGRAM
  (with-slots (lp) itc
    (setf lp (make-numeric-optimization-problem :name 'itc))
    ;; Loop over all events in the problem and make variables for them
    (dolist (e (temporal-network-events network))
      (itc-lp-add-event! itc network e))
    ;; Loop over all constraints in the problem and make constraints
    ;; for them
    (dolist (c (temporal-network-constraints network))
      (itc-lp-add-constraint! itc network c))
    (add-linear-objective! lp))
  (add-itc-callbacks! itc)
  (values))


(defmethod itc-lp-add-event! ((itc itc-linear-program) network (e temporal-event))
  (declare (ignore network))
  (with-slots (lp) itc
    (add-continuous-variable! lp :name (event-name e))))

(defmethod itc-lp-add-constraint! ((itc itc-linear-program) network (c simple-temporal-constraint))
  (declare (ignore network))
  (with-slots (lp) itc
    (let ((c-map (make-linked-hashmap))
          (source-variable (find-variable lp (event-name (temporal-constraint-start c))))
          (target-variable (find-variable lp (event-name (temporal-constraint-end c)))))
      (unless (eq source-variable target-variable)
        (map-add! c-map (find-variable lp (event-name (temporal-constraint-start c))) -1)
        (map-add! c-map (find-variable lp (event-name (temporal-constraint-end c))) 1))
      (setf (gethash c (itc-linear-program-constraint-map itc))
            (add-linear-constraint! lp
                                    :map c-map
                                    :lb (duration-lower-bound (temporal-constraint-duration c))
                                    :ub (duration-upper-bound (temporal-constraint-duration c)))))))

(defmethod itc-lp-remove-constraint! ((itc itc-linear-program) network (c simple-temporal-constraint))
  (declare (ignore network))
  (with-slots (lp) itc
    (let ((lp-c (gethash c (itc-linear-program-constraint-map itc))))
      (setf (constraint-active? lp-c) nil))))
