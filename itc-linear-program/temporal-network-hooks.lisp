;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :itc-v2)

(defmethod add-itc-callbacks! ((itc itc-linear-program))
  (let ((network (itc-temporal-network itc)))
    (add-event-added-callback network #'itc-lp-add-event! :object-ref itc)
    (add-constraint-added-callback network #'itc-lp-add-constraint! :object-ref itc)
    (add-constraint-removed-callback network #'itc-lp-remove-constraint! :object-ref itc)
    (add-duration-lb-changed-callback network #'itc-lp-change-constraint-duration-lower-bound :object-ref itc)
    (add-duration-ub-changed-callback network #'itc-lp-change-constraint-duration-upper-bound :object-ref itc)))

(defmethod itc-lp-change-constraint-duration-upper-bound (itc network (duration bounded-temporal-duration) old-value new-value)
  (declare (ignore old-value network))
  ;; If we got here, we can safely assume this duration is associated
  ;; with an temporal network that is using ITC LP. Look up the
  ;; constraint this duration is associated with and change the bounds
  ;; accordingly.
  (let* ((c (temporal-duration-constraint duration))
         (c-map (itc-linear-program-constraint-map itc))
         (lp-c (gethash c c-map))
         (new-range (- new-value
                       (duration-lower-bound duration))))
    (setf (range lp-c) new-range)))

(defmethod itc-lp-change-constraint-duration-lower-bound (itc network (duration bounded-temporal-duration) old-value new-value)
  (declare (ignore old-value network))
  ;; If we got here, we can safely assume this duration is associated
  ;; with an temporal network that is using ITC LP. Look up the
  ;; constraint this duration is associated with and change the bounds
  ;; accordingly.
  (let* ((c (temporal-duration-constraint duration))
         (c-map (itc-linear-program-constraint-map itc))
         (lp-c (gethash c c-map))
         (new-range (- (duration-upper-bound duration)
                       new-value)))
    (setf (rhs lp-c) new-value)
    (setf (range lp-c) new-range)))
