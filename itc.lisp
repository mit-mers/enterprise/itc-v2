;;;; Copyright (c) 2013 Massachusetts Institute of Technology

;;;; This software may not be redistributed, and can only be retained and used
;;;; with the explicit written consent of the author, subject to the following
;;;; conditions:

;;;; The above copyright notice and this permission notice shall be included in
;;;; all copies or substantial portions of the Software.

;;;; This software may only be used for non-commercial, non-profit, research
;;;; activities.

;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESSED
;;;; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
;;;; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
;;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
;;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :itc-v2)

(defun make-itc (temporal-network &rest init-args &key (itc-method :negative-cycle-detection))
  "Initialize and return an ITC problem.

  + `ITC-METHOD` is one of `:NEGATIVE-CYCLE-DETECTION` (default) or
  `:LINEAR-PROGRAM`."
  (remf init-args :itc-method)
  (let (object)
    (ecase itc-method
      (:linear-program
       (setf object (make-instance 'itc-linear-program :temporal-network temporal-network)))
      (:negative-cycle-detection
       (setf object (make-instance 'itc-negative-cycle-detection :temporal-network temporal-network))))
    (apply #'itc-initialize! object init-args)
    object))

;;; ITC must be initialized on an object before it can be checked for
;;; consistency.

(defgeneric itc-initialize! (temporal-network &key &allow-other-keys)
  (:documentation "Initialize the ITC problem. Re-initializes if it
  has already been initialized with `TEMPORAL-NETWORK`."))
