;; This should get moved to some other file when appropriate

(in-package :itc-v2)

(defun set-debug (state)
  "Enable or disable debug printing."
  (if state
      (enable-itc-debug)
      (disable-itc-debug)))

(declaim (inline enable-itc-debug))
(defun enable-itc-debug ()
  "Enable debugging."
  (log:config
   ;; formatting options
   :notime :nofile :nopretty
   ;; printing level
   :debug))

(declaim (inline disable-itc-debug))
(defun disable-itc-debug ()
  "Disable debugging."
  (log:config
   ;; printing level
   :off))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (log:config
   ;; printing level
   :off))

(defmacro with-output-to-format (stream &rest body)
  "A convenience wrapper to interpret STREAM like the (format) function.
   For example, STREAM can be:
   * t (to write to standard-output and return the last BODY form),
   * nil (to write to a string and return it string),
   * a Stream Object (to write to the stream and return the last BODY form)."
  `(cond (;; we want standard output
          (eq ,stream t)
          (setf ,stream *standard-output*)
          ,@body)
         (;; we want string output
          (eq ,stream nil)
          (with-output-to-string (,stream)
            ,@body))
         (:otherwise
          ,@body)))

(defun with-output-to-format-example (msg stream1)
  "MSG can be any string.
   STREAM1 can be:
   * t (to write MSG to standard-output and return nil),
   * nil (to write MSG to a string and return it string),
   * a Stream object (to write MSG to the Stream and return nil)."
  (with-output-to-format stream1
    (format stream1 "~a" msg)))
